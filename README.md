# Domosnap-customer-management

## Prerequis

*  kubernetes 1.19
*  maven 3
*  OpenJDK 13 (raspberry constraint)
 
## Architecture

### Structure

*   port used = 8081
*   [domain](https://gitlab.com/domosnap/domosnap-customer/-/tree/master/domain "domain"): core business implementation about smartdevice domain.
*   [infra](https://gitlab.com/domosnap/domosnap-customer/-/tree/master/infra "infra"): technical implementation (eventStore, repository, etc..)
*   [rest-adapter](https://gitlab.com/domosnap/domosnap-customer/-/tree/master/rest-adapter "rest-adapter")rest-adapter: external Adapter to communicate with the core busines. In other word: REST API.


## Run

### On your laptop
	mvn clean package
	cd rest-adapter/target
	java -Dquarkus.config.locations=../src/main/configuration/application.properties -jar customer-infra-1.0-SNAPSHOT-runner.jar

## With local docker
    docker build -t customer ./
    docker run -p 8081:8081 customer

### With docker

    docker login registry.gitlab.com
    docker run registry.gitlab.com/domosnap/domosnap-customer/customer:[tag]


### On raspberry cluster

    mvn clean package
    sudo docker build -f Dockerfile_arm32v6 . -t registry.gitlab.com/domosnap/domosnap-customer:latest
    sudo docker push registry.gitlab.com/domosnap/domosnap-customer:latest
    kubectl delete configmap customer
    kubectl create configmap customer --from-file=config.json=./rest-adapter/src/main/configuration/test-configuration.json
    kubectl apply -f customer-arm32v6.yaml


### On pipeline

To be done

### Remark
For new project don't forget to register the token:
    - Create key in gitlab (Project->Settings->Repository->Deploy Tokens with a username and password)
    - On k8s cluster add the key:

    kubectl create secret docker-registry customer-registry-secret --docker-server=registry.gitlab.com --docker-username='gitlab-deploy-token' --docker-password='generated-token'

[More info](https://kubernetes.io/fr/docs/tasks/configure-pod-container/pull-image-private-registry/ "More info")


Don't forget to use imagePullSecret in pod definition: 
    
    imagePullSecrets:
        - name: customer-registry-secret


[API Documentation](https://gitlab.com/domosnap/domosnap-customer/-/blob/master/rest-adapter/src/main/resources/swagger.yaml "API Documentation") 

openssl x509 -pubkey -noout -in <certificat.pem> > <fichier de sortie.pub>

## Lunch MongoDB

## localhost

Lunch:

    sudo docker run --name mongo -d mongo:3

Get IP:

    sudo docker inspect mongo | grep IPAddress

Other:

    sudo docker ps
    sudo docker stop mongo
    sudo docker rm mongo