//package com.domosnap.customer.restadapter;
//
//import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.api.AfterAll;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.junit.jupiter.api.DisplayName;
//import org.junit.jupiter.api.RepeatedTest;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.Order;
//
//import net.leadware.kafka.embedded.KafkaSimulator;
//import net.leadware.kafka.embedded.properties.SimulatorProperties;
//import net.leadware.kafka.embedded.utils.KafkaSimulatorFactory;
//import net.leadware.kafka.embedded.properties.ListenerProperties;
//import net.leadware.kafka.embedded.properties.BrokerProperties;
//
//import static org.assertj.core.api.Assertions.assertThat;
//
//import java.util.List;
//
//public class KafkaTest {
//    static KafkaSimulator kafkaSimulator;
//    private static final int PORT = 9092;
//    private static final String BOOTSTRAP = "PLAINTEXT://127.0.0.1:9092";
//    private static final String GROUP_ID = "group-1";
//    private static final String TOPIC = "customer";
//
//    @BeforeAll
//    public static void initialize() {
//        SimulatorProperties properties = new SimulatorProperties();
//        ListenerProperties listener = new ListenerProperties();
//        listener.setPort(PORT);
//
//        BrokerProperties broker = new BrokerProperties();
//        broker.setListener(listener);
//
//        properties.setBrokerConfigs(List.of(broker));
//        KafkaSimulatorFactory factory = new KafkaSimulatorFactory(properties);
//        kafkaSimulator = factory.getInstance();
//        kafkaSimulator.initialize();
//    }
//
//    @Order(1)
//	@RepeatedTest(3)
//	@DisplayName("Test connection")
//	public void testConnection() throws Throwable {
//        assertThat(kafkaSimulator.getPublicBrokersUrls()).isEqualTo(BOOTSTRAP);
//	}
//
//    @AfterAll
//    public static void close() {
//        kafkaSimulator.destroy();
//    }
//
//}