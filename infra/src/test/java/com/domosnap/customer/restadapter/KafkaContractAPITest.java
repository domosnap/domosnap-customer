//package com.domosnap.customer.restadapter;
//
//import java.util.UUID;
//import java.util.concurrent.TimeUnit;
//
//import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.api.DisplayName;
//import org.junit.jupiter.api.Order;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.junit.jupiter.api.RepeatedTest;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.AfterAll;
//
//import static org.assertj.core.api.Assertions.assertThat;
//
//import io.vertx.core.Vertx;
//import io.vertx.core.json.JsonArray;
//import io.vertx.core.json.JsonObject;
//import io.vertx.junit5.VertxTestContext;
//import io.vertx.junit5.Checkpoint;
//import io.vertx.junit5.Timeout;
//import io.vertx.junit5.VertxExtension;
//import io.vertx.ext.web.client.WebClient;
//import io.vertx.core.DeploymentOptions;
//
//import net.leadware.kafka.embedded.KafkaSimulator;
//import net.leadware.kafka.embedded.properties.SimulatorProperties;
//import net.leadware.kafka.embedded.utils.KafkaSimulatorFactory;
//import net.leadware.kafka.embedded.properties.ListenerProperties;
//import net.leadware.kafka.embedded.properties.BrokerProperties;
//
//import java.util.List;
//
//@ExtendWith(VertxExtension.class)
//public class KafkaContractAPITest {
//    static KafkaSimulator kafkaSimulator;
//    private static final int PORT = 9092;
//    private static final String BOOTSTRAP = "PLAINTEXT://127.0.0.1:9092";
//    private static final String GROUP_ID = "group-1";
//    private static final String TOPIC = "customer";
//
//    private String host = "localhost";
//	private int port = 8081;
//	private String jwt ="eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiT2xpdmllciBEcmllc2JhY2giLCJwaWN0dXJlIjoiaHR0cHM6Ly9saDMuZ29vZ2xldXNlcmNvbnRlbnQuY29tLy1CSVJmT3J5dThVYy9BQUFBQUFBQUFBSS9BQUFBQUFBQUFBQS9BQ0hpM3JlSUVVd21VSldqbTNoWmlFRXZCNjlJb2N1OHN3L3M5Ni1jL3Bob3RvLmpwZyIsImlzcyI6Imh0dHBzOi8vc2VjdXJldG9rZW4uZ29vZ2xlLmNvbS9jdXJhbnRpLW12cCIsImF1ZCI6ImN1cmFudGktbXZwIiwiYXV0aF90aW1lIjoxNTYyMTUyMzU2LCJ1c2VyX2lkIjoibjVNSEcyVEx1cVliemM4QXJoNkx3VkxmaVp1MiIsInN1YiI6Im41TUhHMlRMdXFZYnpjOEFyaDZMd1ZMZmladTIiLCJpYXQiOjE1NjIxNTIzNTYsImVtYWlsIjoib2xpdmllci5kcmllc2JhY2hAY3VyYW50aS5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiZmlyZWJhc2UiOnsiaWRlbnRpdGllcyI6eyJnb29nbGUuY29tIjpbIjExNTI3NDUyODg1NjQ2NDY0NDA1MiJdLCJlbWFpbCI6WyJvbGl2aWVyLmRyaWVzYmFjaEBjdXJhbnRpLmNvbSJdfSwic2lnbl9pbl9wcm92aWRlciI6Imdvb2dsZS5jb20ifX0.OLgwhtHzjhVmJV9U_lVCWkyPlRem5tyK8BIdRw2y7nDQzAkoh9sj2O_7uND7D6qSSgW9FQyEpXSdHfXE-YVYO6yHR_IJAt7ATawVtl4krVuKevDGOC6aVHncbfvYENvs-ADCWKYQ6-teEwC99S7_1iwcD5okuH4BQ2aOQe4XW7-7qVdhDHw0Q3BzPe9wlEoVqS1vebmxXBW-Rgt4Z-1LcMzYKVm5YM2Uiw-vibxAFRD1QbM1pm-3uQYlFrpHy5ksiuh4YoaIeZKiJKZNOEOzJ-XmdtvYwIWmtjZ5gI2iol75kQNJ00A2aF7H2pN90v6DTTxvaAT67-Xoc8stYx1XUA";
//	private Vertx vertx = Vertx.vertx();
//	String uid = UUID.randomUUID().toString();
//	JsonObject Register1 = new JsonObject()
//			.put("idRegister", "reg1")
//			.put("uri", "uri1");
//	JsonObject Register2 = new JsonObject()
//			.put("idRegister", "reg2")
//			.put("uri", "uri2");
//	JsonArray registers = new JsonArray()
//			.add(Register1)
//			.add(Register2);
//	JsonObject MesurePoint1 = new JsonObject()
//			.put("registers", registers);
//	JsonObject MesurePoint2 = new JsonObject()
//			.put("registers", registers);
//	JsonArray MesuresPoints =new JsonArray()
//			.add(MesurePoint1)
//			.add(MesurePoint2);
//	JsonObject Contract1 = new JsonObject()
//			.put("idPartner", 1)
//			.put("idContract", "contract1")
//			.put("type", "contract1")
//			.put("beginDate", 0)
//			.put("endDate", 0)
//			.put("measuresPoints", MesuresPoints);
//	JsonObject Contract2 = new JsonObject()
//			.put("idContract", "contract2")
//			.put("type", "contrat2")
//			.put("beginDate", 0)
//			.put("endDate", 0)
//			.put("measuresPoints", MesuresPoints);
//	JsonArray Contracts =new JsonArray()
//			.add(Contract1)
//			.add(Contract2);
//
//	/**
//	 * this initilize the unit test
//	 * @throws Exception
//	 */
//	@BeforeAll
//	@DisplayName("Deploying customer verticle")
//	public static void initialize(Vertx vertx, VertxTestContext testContext) {
//		SimulatorProperties properties = new SimulatorProperties();
//        ListenerProperties listener = new ListenerProperties();
//        listener.setPort(PORT);
//
//        BrokerProperties broker = new BrokerProperties();
//        broker.setListener(listener);
//
//        properties.setBrokerConfigs(List.of(broker));
//        KafkaSimulatorFactory factory = new KafkaSimulatorFactory(properties);
//        kafkaSimulator = factory.getInstance();
//        kafkaSimulator.initialize();
//
//        DeploymentOptions options = new DeploymentOptions()
//            .setConfig(new JsonObject()
//                .put(ConfigCustomer.CONFIG_EVENT_STORE_KAFKA, "PLAINTEXT://localhost:9092")
//                .put(ConfigCustomer.CONFIG_EVENT_STORE, "com.domosnap.customer.infra.eventstore.KafkaEventStore")
//            );
//		// Start server
//		vertx.deployVerticle(new ServerCustomer(), options, testContext.succeedingThenComplete());
//	}
//
//    @Order(102)
//	@RepeatedTest(3)
//	@Timeout(value = 5, timeUnit = TimeUnit.SECONDS)
//	public void testCreateContract(VertxTestContext testContext) throws Throwable {
//		WebClient client = WebClient.create(vertx);
//        Checkpoint createContractCheckpoint = testContext.checkpoint(2);
//
//        client
//			.put(port, host , "/customers/" + this.uid)
//			.bearerTokenAuthentication(jwt)
//			.putHeader("idPartner", "1")
//			.sendJsonObject(new JsonObject()
//				.put("externalId", "test")
//				.put("contracts", Contracts), testContext.succeeding(ar -> testContext.verify(() -> {
//					assertThat(ar.statusCode()).isEqualTo(201);
//					String[] foundUid = ar.headers().get("Location").split("/");
//					assertThat(foundUid[foundUid.length - 1]).isEqualTo(this.uid);
//					assertThat(ar.bodyAsJsonObject().getString("id")).isEqualTo(this.uid);
//
//                    client
//                        .post(port, host , "/customers/" + this.uid + "/contract")
//                        .bearerTokenAuthentication(jwt)
//                        .putHeader("idPartner", "1")
//                        .sendJsonObject(new JsonObject()
//                            .put("type", "contrat3")
//                            .put("beginDate", 0)
//                            .put("endDate", 0)
//                            .put("measuresPoints", MesuresPoints), testContext.succeeding(ar1 -> testContext.verify(() -> {
//                                assertThat(ar1.statusCode()).isEqualTo(201);
//                                String[] foundUid1 = ar1.headers().get("Location").split("/");
//                                assertThat(foundUid1[foundUid1.length - 1].length() == 36).isEqualTo(true);
//                                assertThat(ar1.bodyAsJsonObject().getString("idContract")).isEqualTo(foundUid1[foundUid1.length - 1]);
//                                createContractCheckpoint.flag();
//                            })));
//
//                    client
//                        .post(port, host , "/customers/" + this.uid + "/contract")
//                        .bearerTokenAuthentication(jwt)
//                        .putHeader("idPartner", "1")
//                        .sendJsonObject(new JsonObject()
//                            .put("type", "contrat3")
//                            .put("beginDate", 0)
//                            .put("measuresPoints", MesuresPoints), testContext.succeeding(ar1 -> testContext.verify(() -> {
//                                assertThat(ar1.statusCode()).isEqualTo(201);
//                                String[] foundUid1 = ar1.headers().get("Location").split("/");
//                                assertThat(foundUid1[foundUid1.length - 1].length() == 36).isEqualTo(true);
//                                assertThat(ar1.bodyAsJsonObject().getString("idContract")).isEqualTo(foundUid1[foundUid1.length - 1]);
//                                createContractCheckpoint.flag();
//                            })));
//				})));
//	}
//
//    @Order(103)
//	@RepeatedTest(3)
//	@Timeout(value = 5, timeUnit = TimeUnit.SECONDS)
//	public void testCreateWrongContract(VertxTestContext testContext) throws Throwable {
//		WebClient client = WebClient.create(vertx);
//        Checkpoint wrongContractCheckpoint = testContext.checkpoint(6);
//
//        client
//			.put(port, host , "/customers/" + this.uid)
//			.bearerTokenAuthentication(jwt)
//			.putHeader("idPartner", "1")
//			.sendJsonObject(new JsonObject()
//				.put("externalId", "test")
//				.put("contracts", Contracts), testContext.succeeding(ar -> testContext.verify(() -> {
//					assertThat(ar.statusCode()).isEqualTo(201);
//					String[] foundUid = ar.headers().get("Location").split("/");
//					assertThat(foundUid[foundUid.length - 1]).isEqualTo(this.uid);
//					assertThat(ar.bodyAsJsonObject().getString("id")).isEqualTo(this.uid);
//
//                    // idPartner is missing
//                    client
//                        .post(port, host , "/customers/" + this.uid + "/contract")
//                        .bearerTokenAuthentication(jwt)
//                        .sendJsonObject(new JsonObject()
//                            .put("type", "contrat3")
//                            .put("beginDate", 0)
//                            .put("endDate", 0)
//                            .put("measuresPoints", MesuresPoints), testContext.succeeding(ar1 -> testContext.verify(() -> {
//                                assertThat(ar1.statusCode()).isEqualTo(400);
//                                wrongContractCheckpoint.flag();
//                            })));
//
//                    // beginDate is missing
//                    client
//                        .post(port, host , "/customers/" + this.uid + "/contract")
//                        .bearerTokenAuthentication(jwt)
//                        .putHeader("idPartner", "1")
//                        .sendJsonObject(new JsonObject()
//                            .put("type", "contrat3")
//                            .put("endDate", 0)
//                            .put("measuresPoints", MesuresPoints), testContext.succeeding(ar1 -> testContext.verify(() -> {
//                                assertThat(ar1.statusCode()).isEqualTo(400);
//                                wrongContractCheckpoint.flag();
//                            })));
//
//                    // measurePoints is missing
//                    client
//                        .post(port, host , "/customers/" + this.uid + "/contract")
//                        .bearerTokenAuthentication(jwt)
//                        .putHeader("idPartner", "1")
//                        .sendJsonObject(new JsonObject()
//                            .put("type", "contrat3")
//                            .put("beginDate", 0)
//                            .put("endDate", 0), testContext.succeeding(ar1 -> testContext.verify(() -> {
//                                assertThat(ar1.statusCode()).isEqualTo(400);
//                                wrongContractCheckpoint.flag();
//                            })));
//
//                    // type is missing
//                    client
//                        .post(port, host , "/customers/" + this.uid + "/contract")
//                        .bearerTokenAuthentication(jwt)
//                        .putHeader("idPartner", "1")
//                        .sendJsonObject(new JsonObject()
//                            .put("beginDate", 0)
//                            .put("endDate", 0)
//                            .put("measuresPoints", MesuresPoints), testContext.succeeding(ar1 -> testContext.verify(() -> {
//                                assertThat(ar1.statusCode()).isEqualTo(400);
//                                wrongContractCheckpoint.flag();
//                            })));
//
//                    // jwt is missing
//                    client
//                        .post(port, host , "/customers/" + this.uid + "/contract")
//                        .putHeader("idPartner", "1")
//                        .sendJsonObject(new JsonObject()
//                            .put("type", "contrat3")
//                            .put("beginDate", 0)
//                            .put("endDate", 0)
//                            .put("measuresPoints", MesuresPoints), testContext.succeeding(ar1 -> testContext.verify(() -> {
//                                assertThat(ar1.statusCode()).isEqualTo(401);
//                                wrongContractCheckpoint.flag();
//                            })));
//
//                    // customer id not existing
//                    client
//                        .post(port, host , "/customers/" + UUID.randomUUID().toString() + "/contract")
//                        .bearerTokenAuthentication(jwt)
//                        .putHeader("idPartner", "1")
//                        .sendJsonObject(new JsonObject()
//                            .put("type", "contrat3")
//                            .put("beginDate", 0)
//                            .put("endDate", 0)
//                            .put("measuresPoints", MesuresPoints), testContext.succeeding(ar1 -> testContext.verify(() -> {
//                                assertThat(ar1.statusCode()).isEqualTo(404);
//                                wrongContractCheckpoint.flag();
//                            })));
//				})));
//
//	}
//
//    @Order(104)
//	@RepeatedTest(3)
//	@Timeout(value = 5, timeUnit = TimeUnit.SECONDS)
//	public void testCreateOrUpdateContract(VertxTestContext testContext) throws Throwable {
//		WebClient client = WebClient.create(vertx);
//        Checkpoint createContractCheckpoint = testContext.checkpoint(2);
//        String contractId = UUID.randomUUID().toString();
//        String contractId1 = UUID.randomUUID().toString();
//
//        client
//			.put(port, host , "/customers/" + this.uid)
//			.bearerTokenAuthentication(jwt)
//			.putHeader("idPartner", "1")
//			.sendJsonObject(new JsonObject()
//				.put("externalId", "test")
//				.put("contracts", Contracts), testContext.succeeding(ar -> testContext.verify(() -> {
//					assertThat(ar.statusCode()).isEqualTo(201);
//					String[] foundUid = ar.headers().get("Location").split("/");
//					assertThat(foundUid[foundUid.length - 1]).isEqualTo(this.uid);
//					assertThat(ar.bodyAsJsonObject().getString("id")).isEqualTo(this.uid);
//
//                    client
//                        .put(port, host , "/customers/" + this.uid + "/contract/" + contractId)
//                        .bearerTokenAuthentication(jwt)
//                        .putHeader("idPartner", "1")
//                        .sendJsonObject(new JsonObject()
//                            .put("type", "contrat3")
//                            .put("beginDate", 0)
//                            .put("endDate", 0)
//                            .put("measuresPoints", MesuresPoints), testContext.succeeding(ar1 -> testContext.verify(() -> {
//                                assertThat(ar1.statusCode()).isEqualTo(201);
//                                String[] foundUid1 = ar1.headers().get("Location").split("/");
//                                assertThat(foundUid1[foundUid1.length - 1]).isEqualTo(contractId);
//                                assertThat(ar1.bodyAsJsonObject().getString("idContract")).isEqualTo(contractId);
//
//                                client
//                                    .put(port, host , "/customers/" + this.uid + "/contract/" + contractId)
//                                    .bearerTokenAuthentication(jwt)
//                                    .putHeader("idPartner", "1")
//                                    .sendJsonObject(new JsonObject()
//                                        .put("type", "contrat3")
//                                        .put("beginDate", 0)
//                                        .put("endDate", 0)
//                                        .put("measuresPoints", MesuresPoints), testContext.succeeding(ar2 -> testContext.verify(() -> {
//                                            assertThat(ar2.statusCode()).isEqualTo(200);
//                                            assertThat(ar2.bodyAsJsonObject().getString("idContract")).isEqualTo(contractId);
//                                            createContractCheckpoint.flag();
//                                        })));
//                            })));
//
//                    client
//                        .put(port, host , "/customers/" + this.uid + "/contract/" + contractId1)
//                        .bearerTokenAuthentication(jwt)
//                        .putHeader("idPartner", "1")
//                        .sendJsonObject(new JsonObject()
//                            .put("type", "contrat3")
//                            .put("beginDate", 0)
//                            .put("measuresPoints", MesuresPoints), testContext.succeeding(ar1 -> testContext.verify(() -> {
//                                assertThat(ar1.statusCode()).isEqualTo(201);
//                                String[] foundUid1 = ar1.headers().get("Location").split("/");
//                                assertThat(foundUid1[foundUid1.length - 1]).isEqualTo(contractId1);
//                                assertThat(ar1.bodyAsJsonObject().getString("idContract")).isEqualTo(contractId1);
//
//                                client
//                                    .put(port, host , "/customers/" + this.uid + "/contract/" + contractId1)
//                                    .bearerTokenAuthentication(jwt)
//                                    .putHeader("idPartner", "1")
//                                    .sendJsonObject(new JsonObject()
//                                        .put("type", "contrat3")
//                                        .put("beginDate", 0)
//                                        .put("measuresPoints", MesuresPoints), testContext.succeeding(ar2 -> testContext.verify(() -> {
//                                            assertThat(ar2.statusCode()).isEqualTo(200);
//                                            assertThat(ar2.bodyAsJsonObject().getString("idContract")).isEqualTo(contractId1);
//                                            createContractCheckpoint.flag();
//                                        })));
//                            })));
//
//				})));
//	}
//
//    @Order(105)
//	@RepeatedTest(3)
//	@Timeout(value = 5, timeUnit = TimeUnit.SECONDS)
//	public void testCreateOrUpdateWrongContract(VertxTestContext testContext) throws Throwable {
//		WebClient client = WebClient.create(vertx);
//        Checkpoint wrongContractCheckpoint = testContext.checkpoint(6);
//        String contractId = UUID.randomUUID().toString();
//
//        client
//			.put(port, host , "/customers/" + this.uid)
//			.bearerTokenAuthentication(jwt)
//			.putHeader("idPartner", "1")
//			.sendJsonObject(new JsonObject()
//				.put("externalId", "test")
//				.put("contracts", Contracts), testContext.succeeding(ar -> testContext.verify(() -> {
//					assertThat(ar.statusCode()).isEqualTo(201);
//					String[] foundUid = ar.headers().get("Location").split("/");
//					assertThat(foundUid[foundUid.length - 1]).isEqualTo(this.uid);
//					assertThat(ar.bodyAsJsonObject().getString("id")).isEqualTo(this.uid);
//
//                    // idPartner is missing
//                    client
//                        .put(port, host , "/customers/" + this.uid + "/contract/" + contractId)
//                        .bearerTokenAuthentication(jwt)
//                        .sendJsonObject(new JsonObject()
//                            .put("type", "contrat3")
//                            .put("beginDate", 0)
//                            .put("endDate", 0)
//                            .put("measuresPoints", MesuresPoints), testContext.succeeding(ar1 -> testContext.verify(() -> {
//                                assertThat(ar1.statusCode()).isEqualTo(400);
//                                wrongContractCheckpoint.flag();
//                            })));
//
//                    // beginDate is missing
//                    client
//                        .put(port, host , "/customers/" + this.uid + "/contract/" + contractId)
//                        .bearerTokenAuthentication(jwt)
//                        .putHeader("idPartner", "1")
//                        .sendJsonObject(new JsonObject()
//                            .put("type", "contrat3")
//                            .put("endDate", 0)
//                            .put("measuresPoints", MesuresPoints), testContext.succeeding(ar1 -> testContext.verify(() -> {
//                                assertThat(ar1.statusCode()).isEqualTo(400);
//                                wrongContractCheckpoint.flag();
//                            })));
//
//                    // measurePoints is missing
//                    client
//                        .put(port, host , "/customers/" + this.uid + "/contract/" + contractId)
//                        .bearerTokenAuthentication(jwt)
//                        .putHeader("idPartner", "1")
//                        .sendJsonObject(new JsonObject()
//                            .put("type", "contrat3")
//                            .put("beginDate", 0)
//                            .put("endDate", 0), testContext.succeeding(ar1 -> testContext.verify(() -> {
//                                assertThat(ar1.statusCode()).isEqualTo(400);
//                                wrongContractCheckpoint.flag();
//                            })));
//
//                    // type is missing
//                    client
//                        .put(port, host , "/customers/" + this.uid + "/contract/" + contractId)
//                        .bearerTokenAuthentication(jwt)
//                        .putHeader("idPartner", "1")
//                        .sendJsonObject(new JsonObject()
//                            .put("beginDate", 0)
//                            .put("endDate", 0)
//                            .put("measuresPoints", MesuresPoints), testContext.succeeding(ar1 -> testContext.verify(() -> {
//                                assertThat(ar1.statusCode()).isEqualTo(400);
//                                wrongContractCheckpoint.flag();
//                            })));
//
//                    // jwt is missing
//                    client
//                        .put(port, host , "/customers/" + this.uid + "/contract/" + contractId)
//                        .putHeader("idPartner", "1")
//                        .sendJsonObject(new JsonObject()
//                            .put("type", "contrat3")
//                            .put("beginDate", 0)
//                            .put("endDate", 0)
//                            .put("measuresPoints", MesuresPoints), testContext.succeeding(ar1 -> testContext.verify(() -> {
//                                assertThat(ar1.statusCode()).isEqualTo(401);
//                                wrongContractCheckpoint.flag();
//                            })));
//
//                    // customer id not existing
//                    client
//                        .put(port, host , "/customers/" + UUID.randomUUID().toString() + "/contract/" + contractId)
//                        .bearerTokenAuthentication(jwt)
//                        .putHeader("idPartner", "1")
//                        .sendJsonObject(new JsonObject()
//                            .put("type", "contrat3")
//                            .put("beginDate", 0)
//                            .put("endDate", 0)
//                            .put("measuresPoints", MesuresPoints), testContext.succeeding(ar1 -> testContext.verify(() -> {
//                                assertThat(ar1.statusCode()).isEqualTo(404);
//                                wrongContractCheckpoint.flag();
//                            })));
//				})));
//
//	}
//
//    @Order(106)
//	@RepeatedTest(3)
//	@Timeout(value = 5, timeUnit = TimeUnit.SECONDS)
//	public void testGetContract(VertxTestContext testContext) throws Throwable {
//		WebClient client = WebClient.create(vertx);
//        String contractId = UUID.randomUUID().toString();
//
//        client
//			.put(port, host , "/customers/" + this.uid)
//			.bearerTokenAuthentication(jwt)
//			.putHeader("idPartner", "1")
//			.sendJsonObject(new JsonObject()
//				.put("externalId", "test")
//				.put("contracts", Contracts), testContext.succeeding(ar -> testContext.verify(() -> {
//					assertThat(ar.statusCode()).isEqualTo(201);
//					String[] foundUid = ar.headers().get("Location").split("/");
//					assertThat(foundUid[foundUid.length - 1]).isEqualTo(this.uid);
//					assertThat(ar.bodyAsJsonObject().getString("id")).isEqualTo(this.uid);
//
//                    client
//                        .put(port, host , "/customers/" + this.uid + "/contract/" + contractId)
//                        .bearerTokenAuthentication(jwt)
//                        .putHeader("idPartner", "1")
//                        .sendJsonObject(new JsonObject()
//                            .put("type", "contrat3")
//                            .put("beginDate", 0)
//                            .put("endDate", 0)
//                            .put("measuresPoints", MesuresPoints), testContext.succeeding(ar1 -> testContext.verify(() -> {
//                                assertThat(ar1.statusCode()).isEqualTo(201);
//                                String[] foundUid1 = ar1.headers().get("Location").split("/");
//                                assertThat(foundUid1[foundUid1.length - 1]).isEqualTo(contractId);
//                                assertThat(ar1.bodyAsJsonObject().getString("idContract")).isEqualTo(contractId);
//
//                                client
//                                    .get(port, host , "/customers/" + this.uid + "/contract/" + contractId)
//                                    .putHeader("idPartner", "1")
//                                    .bearerTokenAuthentication(jwt)
//                                    .send(testContext.succeeding(ar2 -> testContext.verify(() -> {
//                                        assertThat(ar2.statusCode()).isEqualTo(200);
//                                        assertThat(ar2.bodyAsJsonObject().getString("idContract")).isEqualTo(contractId);
//                                        testContext.completeNow();
//                                    })));
//                            })));
//				})));
//	}
//
//    @Order(107)
//	@RepeatedTest(3)
//	@Timeout(value = 5, timeUnit = TimeUnit.SECONDS)
//	public void testGetWrongContract(VertxTestContext testContext) throws Throwable {
//		WebClient client = WebClient.create(vertx);
//        String contractId = UUID.randomUUID().toString();
//        Checkpoint wrongContractCheckpoint = testContext.checkpoint(4);
//
//        client
//			.put(port, host , "/customers/" + this.uid)
//			.bearerTokenAuthentication(jwt)
//			.putHeader("idPartner", "1")
//			.sendJsonObject(new JsonObject()
//				.put("externalId", "test")
//				.put("contracts", Contracts), testContext.succeeding(ar -> testContext.verify(() -> {
//					assertThat(ar.statusCode()).isEqualTo(201);
//					String[] foundUid = ar.headers().get("Location").split("/");
//					assertThat(foundUid[foundUid.length - 1]).isEqualTo(this.uid);
//					assertThat(ar.bodyAsJsonObject().getString("id")).isEqualTo(this.uid);
//
//                    client
//                        .put(port, host , "/customers/" + this.uid + "/contract/" + contractId)
//                        .bearerTokenAuthentication(jwt)
//                        .putHeader("idPartner", "1")
//                        .sendJsonObject(new JsonObject()
//                            .put("type", "contrat3")
//                            .put("beginDate", 0)
//                            .put("endDate", 0)
//                            .put("measuresPoints", MesuresPoints), testContext.succeeding(ar1 -> testContext.verify(() -> {
//                                assertThat(ar1.statusCode()).isEqualTo(201);
//                                String[] foundUid1 = ar1.headers().get("Location").split("/");
//                                assertThat(foundUid1[foundUid1.length - 1]).isEqualTo(contractId);
//                                assertThat(ar1.bodyAsJsonObject().getString("idContract")).isEqualTo(contractId);
//
//                                // idCustomer non existing
//                                client
//                                    .get(port, host , "/customers/" + UUID.randomUUID().toString() + "/contract/" + contractId)
//                                    .putHeader("idPartner", "1")
//                                    .bearerTokenAuthentication(jwt)
//                                    .send(testContext.succeeding(ar2 -> testContext.verify(() -> {
//                                        assertThat(ar2.statusCode()).isEqualTo(404);
//                                        wrongContractCheckpoint.flag();
//                                    })));
//
//                                // contract non existing
//                                client
//                                    .get(port, host , "/customers/" + this.uid + "/contract/" + UUID.randomUUID().toString())
//                                    .putHeader("idPartner", "1")
//                                    .bearerTokenAuthentication(jwt)
//                                    .send(testContext.succeeding(ar2 -> testContext.verify(() -> {
//                                        assertThat(ar2.statusCode()).isEqualTo(404);
//                                        wrongContractCheckpoint.flag();
//                                    })));
//
//                                // idPartner missing
//                                client
//                                    .get(port, host , "/customers/" + this.uid + "/contract/" + contractId)
//                                    .bearerTokenAuthentication(jwt)
//                                    .send(testContext.succeeding(ar2 -> testContext.verify(() -> {
//                                        assertThat(ar2.statusCode()).isEqualTo(400);
//                                        wrongContractCheckpoint.flag();
//                                    })));
//
//                                // jwt is missing
//                                client
//                                    .get(port, host , "/customers/" + this.uid + "/contract/" + contractId)
//                                    .putHeader("idPartner", "1")
//                                    .send(testContext.succeeding(ar2 -> testContext.verify(() -> {
//                                        assertThat(ar2.statusCode()).isEqualTo(401);
//                                        wrongContractCheckpoint.flag();
//                                    })));
//                            })));
//				})));
//	}
//
//    @Order(108)
//	@RepeatedTest(3)
//	@Timeout(value = 5, timeUnit = TimeUnit.SECONDS)
//	public void testDeleteContract(VertxTestContext testContext) throws Throwable {
//		WebClient client = WebClient.create(vertx);
//        String contractId = UUID.randomUUID().toString();
//
//        client
//			.put(port, host , "/customers/" + this.uid)
//			.bearerTokenAuthentication(jwt)
//			.putHeader("idPartner", "1")
//			.sendJsonObject(new JsonObject()
//				.put("externalId", "test")
//				.put("contracts", Contracts), testContext.succeeding(ar -> testContext.verify(() -> {
//					assertThat(ar.statusCode()).isEqualTo(201);
//					String[] foundUid = ar.headers().get("Location").split("/");
//					assertThat(foundUid[foundUid.length - 1]).isEqualTo(this.uid);
//					assertThat(ar.bodyAsJsonObject().getString("id")).isEqualTo(this.uid);
//
//                    client
//                        .put(port, host , "/customers/" + this.uid + "/contract/" + contractId)
//                        .bearerTokenAuthentication(jwt)
//                        .putHeader("idPartner", "1")
//                        .sendJsonObject(new JsonObject()
//                            .put("type", "contrat3")
//                            .put("beginDate", 0)
//                            .put("endDate", 0)
//                            .put("measuresPoints", MesuresPoints), testContext.succeeding(ar1 -> testContext.verify(() -> {
//                                assertThat(ar1.statusCode()).isEqualTo(201);
//                                String[] foundUid1 = ar1.headers().get("Location").split("/");
//                                assertThat(foundUid1[foundUid1.length - 1]).isEqualTo(contractId);
//                                assertThat(ar1.bodyAsJsonObject().getString("idContract")).isEqualTo(contractId);
//
//                                client
//                                    .get(port, host , "/customers/" + this.uid + "/contract/" + contractId)
//                                    .putHeader("idPartner", "1")
//                                    .bearerTokenAuthentication(jwt)
//                                    .send(testContext.succeeding(ar2 -> testContext.verify(() -> {
//                                        assertThat(ar2.statusCode()).isEqualTo(200);
//                                        assertThat(ar2.bodyAsJsonObject().getString("idContract")).isEqualTo(contractId);
//
//                                        client
//                                        .delete(port, host , "/customers/" + this.uid + "/contract/" + contractId)
//                                        .putHeader("idPartner", "1")
//                                        .bearerTokenAuthentication(jwt)
//                                        .send(testContext.succeeding(ar3 -> testContext.verify(() -> {
//                                            assertThat(ar3.statusCode()).isEqualTo(204);
//                                            testContext.completeNow();
//                                        })));
//                                    })));
//                            })));
//				})));
//	}
//
//    @Order(109)
//	@RepeatedTest(3)
//	@Timeout(value = 5, timeUnit = TimeUnit.SECONDS)
//	public void testDeleteWrongContract(VertxTestContext testContext) throws Throwable {
//		WebClient client = WebClient.create(vertx);
//        String contractId = UUID.randomUUID().toString();
//        Checkpoint wrongContractCheckpoint = testContext.checkpoint(4);
//
//        client
//			.put(port, host , "/customers/" + this.uid)
//			.bearerTokenAuthentication(jwt)
//			.putHeader("idPartner", "1")
//			.sendJsonObject(new JsonObject()
//				.put("externalId", "test")
//				.put("contracts", Contracts), testContext.succeeding(ar -> testContext.verify(() -> {
//					assertThat(ar.statusCode()).isEqualTo(201);
//					String[] foundUid = ar.headers().get("Location").split("/");
//					assertThat(foundUid[foundUid.length - 1]).isEqualTo(this.uid);
//					assertThat(ar.bodyAsJsonObject().getString("id")).isEqualTo(this.uid);
//
//                    client
//                        .put(port, host , "/customers/" + this.uid + "/contract/" + contractId)
//                        .bearerTokenAuthentication(jwt)
//                        .putHeader("idPartner", "1")
//                        .sendJsonObject(new JsonObject()
//                            .put("type", "contrat3")
//                            .put("beginDate", 0)
//                            .put("endDate", 0)
//                            .put("measuresPoints", MesuresPoints), testContext.succeeding(ar1 -> testContext.verify(() -> {
//                                assertThat(ar1.statusCode()).isEqualTo(201);
//                                String[] foundUid1 = ar1.headers().get("Location").split("/");
//                                assertThat(foundUid1[foundUid1.length - 1]).isEqualTo(contractId);
//                                assertThat(ar1.bodyAsJsonObject().getString("idContract")).isEqualTo(contractId);
//
//                                client
//                                    .get(port, host , "/customers/" + this.uid + "/contract/" + contractId)
//                                    .putHeader("idPartner", "1")
//                                    .bearerTokenAuthentication(jwt)
//                                    .send(testContext.succeeding(ar2 -> testContext.verify(() -> {
//                                        assertThat(ar2.statusCode()).isEqualTo(200);
//                                        assertThat(ar2.bodyAsJsonObject().getString("idContract")).isEqualTo(contractId);
//
//                                        // idCustomer non existing
//                                        client
//                                            .delete(port, host , "/customers/" + UUID.randomUUID().toString() + "/contract/" + contractId)
//                                            .putHeader("idPartner", "1")
//                                            .bearerTokenAuthentication(jwt)
//                                            .send(testContext.succeeding(ar3 -> testContext.verify(() -> {
//                                                assertThat(ar3.statusCode()).isEqualTo(404);
//                                                wrongContractCheckpoint.flag();
//                                            })));
//
//                                        // contract non existing
//                                        client
//                                            .delete(port, host , "/customers/" + this.uid + "/contract/" + UUID.randomUUID().toString())
//                                            .putHeader("idPartner", "1")
//                                            .bearerTokenAuthentication(jwt)
//                                            .send(testContext.succeeding(ar3 -> testContext.verify(() -> {
//                                                assertThat(ar3.statusCode()).isEqualTo(404);
//                                                wrongContractCheckpoint.flag();
//                                            })));
//
//                                        // idPartner missing
//                                        client
//                                            .delete(port, host , "/customers/" + this.uid + "/contract/" + contractId)
//                                            .bearerTokenAuthentication(jwt)
//                                            .send(testContext.succeeding(ar3 -> testContext.verify(() -> {
//                                                assertThat(ar3.statusCode()).isEqualTo(400);
//                                                wrongContractCheckpoint.flag();
//                                            })));
//
//                                        // jwt is missing
//                                        client
//                                            .delete(port, host , "/customers/" + this.uid + "/contract/" + contractId)
//                                            .putHeader("idPartner", "1")
//                                            .send(testContext.succeeding(ar3 -> testContext.verify(() -> {
//                                                assertThat(ar3.statusCode()).isEqualTo(401);
//                                                wrongContractCheckpoint.flag();
//                                            })));
//                                    })));
//                            })));
//				})));
//	}
//
//    @AfterAll
//    public static void close() {
//        kafkaSimulator.destroy();
//    }
//}
