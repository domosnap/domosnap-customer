package com.domosnap.customer.restadapter;

import io.quarkus.test.junit.QuarkusTest;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.TestMethodOrder;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.equalTo;


@QuarkusTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CustomerQuarkusAPITest {

    private final String jwt = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6Ik9saXZpZXIgRHJpZXNiYWNoIiwiaWF0IjoxNTE2MjM5MDIyLCJleHAiOjIwMTYyMzkwMjIsImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6ODA4MC9yZWFsbXMvbWFzdGVyIiwiYXVkIjoic21hcnRkZXZpY2UtcmVzdCIsInVzZXJfaWQiOiJuNU1IRzJUTHVxWWJ6YzhBcmg2THdWTGZpWnUyIiwiZW1haWwiOiJvbGl2aWVyLmRyaWVzYmFjaEBjdXJhbnRpLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlfQ.DzUZ7R0Xk2A6nAwa3rM5WOk804YNd8Tjvo3yBYjBQUxlMb8vusTlMD07pJjahg3zMXgtofogvg_F2TiAl67dKUVyxUeebUvTBvxZlg6tI5OSS-0mKvs4wX1fSk70H6hf9Axl3MuX0DCR--ln7VRGYTNqPsFY5MkPlWQ8Amv2RVdxxv6FIlEGrPmaXdSwJcB183H67hGIMktLVsPckx8lNuILwHKCaqZ3RPEf1YeqYivlkAWkkfRA-YQQPD8sX1-tRbWeL1AQe41NDe4r5xvTGwRc0Waetva1NHWReWxeYACBe7uH9_lgwjDXWrix0qWrUOGB3DfjulBtZUpRiQVI-Q";

    String uid = UUID.randomUUID().toString();
    String uid1 = UUID.randomUUID().toString();
    JsonObject Register1 = new JsonObject()
            .put("idRegister", "reg1")
            .put("uri", "uri1");
    JsonObject Register2 = new JsonObject()
            .put("idRegister", "reg2")
            .put("uri", "uri2");
    JsonArray registers = new JsonArray()
            .add(Register1)
            .add(Register2);
    JsonObject MesurePoint1 = new JsonObject()
            .put("registers", registers);
    JsonObject MesurePoint2 = new JsonObject()
            .put("registers", registers);
    JsonArray MesuresPoints =new JsonArray()
            .add(MesurePoint1)
            .add(MesurePoint2);
    JsonObject Contract1 = new JsonObject()
            .put("idPartner", 1)
            .put("idContract", UUID.randomUUID().toString())
            .put("type", "contract1")
            .put("beginDate", 0)
            .put("endDate", 0)
            .put("measuresPoints", MesuresPoints);
    JsonObject Contract2 = new JsonObject()
            .put("idContract", UUID.randomUUID().toString())
            .put("type", "contrat2")
            .put("beginDate", 0)
            .put("endDate", 0)
            .put("measuresPoints", MesuresPoints);
    JsonArray Contracts =new JsonArray()
            .add(Contract1)
            .add(Contract2);
    JsonObject Contract3 = new JsonObject()
            .put("idPartner", 1)
            .put("idContract", UUID.randomUUID().toString())
            .put("type", "contract3")
            .put("beginDate", 0)
            .put("endDate", 0)
            .put("measuresPoints", MesuresPoints);
    JsonObject Contract4 = new JsonObject()
            .put("idContract", UUID.randomUUID().toString())
            .put("type", "contrat3")
            .put("beginDate", 0)
            .put("endDate", 0)
            .put("measuresPoints", MesuresPoints);
    JsonArray Contracts1 = new JsonArray()
            .add(Contract3)
            .add(Contract4);

    @Test
    @DisplayName("Test Creation Customer")
    @Order(1)
    public void testCreateCustomer() {

        given()
                .auth().oauth2(jwt)
                .contentType(ContentType.JSON)
                .header("idPartner", "1")
                .body(new JsonObject()
                        .put("externalId", "test")
                        .put("contracts", Contracts)
                        .toString())
                .when()
                .post("/customers")
                .then()
                .statusCode(201)
                .and()
                .extract().path("id");

        given()
                .auth().oauth2(jwt)
                .when().get("/customers")
                .then()
                .statusCode(200)
                .body("$.size()", equalTo(2));

    }

    @Test
    @DisplayName("Test Update Customer")
    @Order(2)
    public void testUpdateCustomer() {

        String uid3 = given()
                .auth().oauth2(jwt)
                .when().get("/customers")
                .then()
                .statusCode(200)
                .extract().jsonPath().getString("[0].id");

        JsonObject updatedCustomerData = new JsonObject()
                .put("customerId", uid3)
                .put("externalId", "updatedTest")
                .put("contracts", Contracts1);

        // Update the customer with the new data
        given()
                .auth().oauth2(jwt)
                .contentType(ContentType.JSON)
                .header("idPartner", "1")
                .body(updatedCustomerData.toString())
                .when()
                .put("/customers/" + uid3)
                .then()
                .statusCode(200)
                .body("contracts.size()", equalTo(2));

    }

    @Test
    @DisplayName("Test Delete Customer")
    @Order(3)
    public void testDeleteCustomer() {

        String uid4 = given()
                .auth().oauth2(jwt)
                .when().get("/customers")
                .then()
                .statusCode(200)
                .extract().jsonPath().getString("[0].id");

        given()
                .auth().oauth2(jwt)
                .contentType(ContentType.JSON)
                .when().delete("/customers/" + uid4)
                .then()
                .statusCode(204);
    }


}
