package com.domosnap.customer.restadapter;


import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.junit.jupiter.api.*;

import java.util.UUID;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@QuarkusTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ContractAPIQuarkusTest {
    private final String jwt = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6Ik9saXZpZXIgRHJpZXNiYWNoIiwiaWF0IjoxNTE2MjM5MDIyLCJleHAiOjIwMTYyMzkwMjIsImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6ODA4MC9yZWFsbXMvbWFzdGVyIiwiYXVkIjoic21hcnRkZXZpY2UtcmVzdCIsInVzZXJfaWQiOiJuNU1IRzJUTHVxWWJ6YzhBcmg2THdWTGZpWnUyIiwiZW1haWwiOiJvbGl2aWVyLmRyaWVzYmFjaEBjdXJhbnRpLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlfQ.DzUZ7R0Xk2A6nAwa3rM5WOk804YNd8Tjvo3yBYjBQUxlMb8vusTlMD07pJjahg3zMXgtofogvg_F2TiAl67dKUVyxUeebUvTBvxZlg6tI5OSS-0mKvs4wX1fSk70H6hf9Axl3MuX0DCR--ln7VRGYTNqPsFY5MkPlWQ8Amv2RVdxxv6FIlEGrPmaXdSwJcB183H67hGIMktLVsPckx8lNuILwHKCaqZ3RPEf1YeqYivlkAWkkfRA-YQQPD8sX1-tRbWeL1AQe41NDe4r5xvTGwRc0Waetva1NHWReWxeYACBe7uH9_lgwjDXWrix0qWrUOGB3DfjulBtZUpRiQVI-Q";

    String uid = UUID.randomUUID().toString();
    String uid1 = UUID.randomUUID().toString();

    String key1 = "id1";
    String key2 = "id2";
    String key3 = "id3";
    String key4 = "id4";
    JsonObject Register1 = new JsonObject()
            .put("idRegister", "reg1")
            .put("uri", "uri1");
    JsonObject Register2 = new JsonObject()
            .put("idRegister", "reg2")
            .put("uri", "uri2");
    JsonArray registers = new JsonArray()
            .add(Register1)
            .add(Register2);
    JsonObject MesurePoint1 = new JsonObject()
            .put("registers", registers);
    JsonObject MesurePoint2 = new JsonObject()
            .put("registers", registers);
    JsonArray MesuresPoints =new JsonArray()
            .add(MesurePoint1)
            .add(MesurePoint2);
    JsonObject Contract1 = new JsonObject()
            .put("idPartner", 1)
            .put("idContract", key1)
            .put("type", "contract1")
            .put("beginDate", 0)
            .put("endDate", 0)
            .put("measuresPoints", MesuresPoints);
    JsonObject Contract2 = new JsonObject()
            .put("idContract", key2)
            .put("type", "contrat2")
            .put("beginDate", 0)
            .put("endDate", 0)
            .put("measuresPoints", MesuresPoints);
    JsonArray Contracts =new JsonArray()
            .add(Contract1)
            .add(Contract2);
    JsonObject Contract3 = new JsonObject()
            .put("idPartner", 1)
            .put("idContract", key3)
            .put("type", "contract3")
            .put("beginDate", 0)
            .put("endDate", 0)
            .put("measuresPoints", MesuresPoints);
    JsonObject Contract4 = new JsonObject()
            .put("idContract", key4)
            .put("type", "contract4")
            .put("beginDate", 0)
            .put("endDate", 0)
            .put("measuresPoints", MesuresPoints);
    JsonArray Contracts1 = new JsonArray()
            .add(Contract3)
            .add(Contract4);



    @Test
    @DisplayName("Test Creation Customer")
    @Order(1)
    public void testCreateCustomer() {

        given()
                .auth().oauth2(jwt)
                .contentType(ContentType.JSON)
                .header("idPartner", "1")
                .body(new JsonObject()
                        .put("externalId", "test")
                        .put("contracts", Contracts)
                        .toString())
                .when()
                .post("/customers")
                .then()
                .statusCode(201)
                .and()
                .extract().path("id");

        given()
                .auth().oauth2(jwt)
                .when().get("/customers")
                .then()
                .statusCode(200)
                .body("$.size()", equalTo(1));

    }


    @Test
    @DisplayName("Test Ajout Contrat")
    @Order(2)
    public void testAddInvalidContract() {

        String uid3 = given()
                .auth().oauth2(jwt)
                .when().get("/customers")
                .then()
                .statusCode(200)
                .extract().jsonPath().getString("[0].id");

        given()
                .auth().oauth2(jwt)
                .contentType(ContentType.JSON)
                .body(Contract3.encode())
                .when()
                .post("/customers/" + uid3 + "/contract")
                .then()
                .statusCode(400);
    }

    @Test
    @DisplayName("Test Ajout Contrat")
    @Order(3)
    public void testAddValidContract() {

        String uid3 = given()
                .auth().oauth2(jwt)
                .when().get("/customers")
                .then()
                .statusCode(200)
                .extract().jsonPath().getString("[0].id");

        given()
                .auth().oauth2(jwt)
                .contentType(ContentType.JSON)
                .header("idPartner", "1")
                .body(Contract3.encode())
                .when()
                .post("/customers/" + uid3 + "/contract")
                .then()
                .statusCode(201)
                .body("type", equalTo("contract3"));
    }

//    @Test
//    @DisplayName("Test Update Contract - Succès")
//    @Order(4)
//    public void testUpdateContract() {
//
//        String uid3 = given()
//                .auth().oauth2(jwt)
//                .when().get("/customers")
//                .then()
//                .statusCode(200)
//                .extract().jsonPath().getString("[0].id");
//
//
//        given()
//                .auth().oauth2(jwt)
//                .when()
//                .get("/customers/" + uid3 + "/contracts")
//                .then()
//                .statusCode(200)
//                .body("find { it.idContract == '" + key3 + "' }.type", equalTo("contract3"));
//
//        // 3. Ensuite seulement, faire la mise à jour
//        JsonObject updatedContractData = new JsonObject()
//                .put("idPartner", 1)
//                .put("idContract", key3)
//                .put("idCustomer", uid3)
//                .put("type", "contract3Update")
//                .put("beginDate", System.currentTimeMillis())
//                .put("endDate", System.currentTimeMillis() + 86400000)
//                .put("measuresPoints", MesuresPoints);  // Utiliser le même format que lors de la création
//
//        given()
//                .auth().oauth2(jwt)
//                .contentType(ContentType.JSON)
//                .header("idPartner", "1")
//                .body(updatedContractData.encode())
//                .when()
//                .put("/customers/" + uid3 + "/contract/" + key3)
//                .then()
//                .statusCode(200)
//                .body("idContract", equalTo(key3))
//                .body("type", equalTo("contract3Update"));
//    }


    @Test
    @DisplayName("Test Delete Contract - Succès 204")
    @Order(6)
    public void testDeleteContract() {

        String uid3 = given()
                .auth().oauth2(jwt)
                .when().get("/customers")
                .then()
                .statusCode(200)
                .extract().jsonPath().getString("[0].id");


        given()
                .auth().oauth2(jwt)
                .header("idPartner", "1")
                .when()
                .delete("/customers/" + uid3 + "/contract/" + key2)
                .then()
                .statusCode(204);

    }










}
