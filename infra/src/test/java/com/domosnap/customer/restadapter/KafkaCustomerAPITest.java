//package com.domosnap.customer.restadapter;
//
//import java.util.UUID;
//import java.util.concurrent.TimeUnit;
//
//import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.api.Order;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.junit.jupiter.api.DisplayName;
//import org.junit.jupiter.api.RepeatedTest;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.AfterAll;
//
//import static org.assertj.core.api.Assertions.assertThat;
//
//import io.vertx.core.Vertx;
//import io.vertx.core.json.JsonArray;
//import io.vertx.core.json.JsonObject;
//import io.vertx.junit5.VertxTestContext;
//import io.vertx.junit5.Checkpoint;
//import io.vertx.junit5.Timeout;
//import io.vertx.junit5.VertxExtension;
//import io.vertx.ext.web.client.WebClient;
//import io.vertx.core.DeploymentOptions;
//
//import net.leadware.kafka.embedded.KafkaSimulator;
//import net.leadware.kafka.embedded.properties.SimulatorProperties;
//import net.leadware.kafka.embedded.utils.KafkaSimulatorFactory;
//import net.leadware.kafka.embedded.properties.ListenerProperties;
//import net.leadware.kafka.embedded.properties.BrokerProperties;
//
//import java.util.List;
//
//@ExtendWith(VertxExtension.class)
//public class KafkaCustomerAPITest {
//    static KafkaSimulator kafkaSimulator;
//    private static final int PORT = 9092;
//    private static final String BOOTSTRAP = "PLAINTEXT://127.0.0.1:9092";
//    private static final String GROUP_ID = "group-1";
//    private static final String TOPIC = "customer";
//
//	private String host = "localhost";
//	private int port = 8081;
//	private String jwt ="eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiT2xpdmllciBEcmllc2JhY2giLCJwaWN0dXJlIjoiaHR0cHM6Ly9saDMuZ29vZ2xldXNlcmNvbnRlbnQuY29tLy1CSVJmT3J5dThVYy9BQUFBQUFBQUFBSS9BQUFBQUFBQUFBQS9BQ0hpM3JlSUVVd21VSldqbTNoWmlFRXZCNjlJb2N1OHN3L3M5Ni1jL3Bob3RvLmpwZyIsImlzcyI6Imh0dHBzOi8vc2VjdXJldG9rZW4uZ29vZ2xlLmNvbS9jdXJhbnRpLW12cCIsImF1ZCI6ImN1cmFudGktbXZwIiwiYXV0aF90aW1lIjoxNTYyMTUyMzU2LCJ1c2VyX2lkIjoibjVNSEcyVEx1cVliemM4QXJoNkx3VkxmaVp1MiIsInN1YiI6Im41TUhHMlRMdXFZYnpjOEFyaDZMd1ZMZmladTIiLCJpYXQiOjE1NjIxNTIzNTYsImVtYWlsIjoib2xpdmllci5kcmllc2JhY2hAY3VyYW50aS5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiZmlyZWJhc2UiOnsiaWRlbnRpdGllcyI6eyJnb29nbGUuY29tIjpbIjExNTI3NDUyODg1NjQ2NDY0NDA1MiJdLCJlbWFpbCI6WyJvbGl2aWVyLmRyaWVzYmFjaEBjdXJhbnRpLmNvbSJdfSwic2lnbl9pbl9wcm92aWRlciI6Imdvb2dsZS5jb20ifX0.OLgwhtHzjhVmJV9U_lVCWkyPlRem5tyK8BIdRw2y7nDQzAkoh9sj2O_7uND7D6qSSgW9FQyEpXSdHfXE-YVYO6yHR_IJAt7ATawVtl4krVuKevDGOC6aVHncbfvYENvs-ADCWKYQ6-teEwC99S7_1iwcD5okuH4BQ2aOQe4XW7-7qVdhDHw0Q3BzPe9wlEoVqS1vebmxXBW-Rgt4Z-1LcMzYKVm5YM2Uiw-vibxAFRD1QbM1pm-3uQYlFrpHy5ksiuh4YoaIeZKiJKZNOEOzJ-XmdtvYwIWmtjZ5gI2iol75kQNJ00A2aF7H2pN90v6DTTxvaAT67-Xoc8stYx1XUA";
//	private Vertx vertx = Vertx.vertx();
//	String uid = UUID.randomUUID().toString();
//	String uid1 = UUID.randomUUID().toString();
//	JsonObject Register1 = new JsonObject()
//			.put("idRegister", "reg1")
//			.put("uri", "uri1");
//	JsonObject Register2 = new JsonObject()
//			.put("idRegister", "reg2")
//			.put("uri", "uri2");
//	JsonArray registers = new JsonArray()
//			.add(Register1)
//			.add(Register2);
//	JsonObject MesurePoint1 = new JsonObject()
//			.put("registers", registers);
//	JsonObject MesurePoint2 = new JsonObject()
//			.put("registers", registers);
//	JsonArray MesuresPoints =new JsonArray()
//			.add(MesurePoint1)
//			.add(MesurePoint2);
//	JsonObject Contract1 = new JsonObject()
//			.put("idPartner", 1)
//			.put("idContract", UUID.randomUUID().toString())
//			.put("type", "contract1")
//			.put("beginDate", 0)
//			.put("endDate", 0)
//			.put("measuresPoints", MesuresPoints);
//	JsonObject Contract2 = new JsonObject()
//			.put("idContract", UUID.randomUUID().toString())
//			.put("type", "contrat2")
//			.put("beginDate", 0)
//			.put("endDate", 0)
//			.put("measuresPoints", MesuresPoints);
//	JsonArray Contracts =new JsonArray()
//			.add(Contract1)
//			.add(Contract2);
//	JsonObject Contract3 = new JsonObject()
//		.put("idPartner", 1)
//		.put("idContract", UUID.randomUUID().toString())
//		.put("type", "contract3")
//		.put("beginDate", 0)
//		.put("endDate", 0)
//		.put("measuresPoints", MesuresPoints);
//	JsonObject Contract4 = new JsonObject()
//			.put("idContract", UUID.randomUUID().toString())
//			.put("type", "contrat3")
//			.put("beginDate", 0)
//			.put("endDate", 0)
//			.put("measuresPoints", MesuresPoints);
//	JsonArray Contracts1 = new JsonArray()
//			.add(Contract3)
//			.add(Contract4);
//
//	/**
//	 * this initilize the unit test
//	 * @throws Exception
//	 */
//	@BeforeAll
//	@DisplayName("Deploying customer verticle")
//	public static void initialize(Vertx vertx, VertxTestContext testContext) {
//        SimulatorProperties properties = new SimulatorProperties();
//        ListenerProperties listener = new ListenerProperties();
//        listener.setPort(PORT);
//
//        BrokerProperties broker = new BrokerProperties();
//        broker.setListener(listener);
//
//        properties.setBrokerConfigs(List.of(broker));
//        KafkaSimulatorFactory factory = new KafkaSimulatorFactory(properties);
//        kafkaSimulator = factory.getInstance();
//        kafkaSimulator.initialize();
//
//        DeploymentOptions options = new DeploymentOptions()
//            .setConfig(new JsonObject()
//                .put(ConfigCustomer.CONFIG_EVENT_STORE_KAFKA, "PLAINTEXT://localhost:9092")
//                .put(ConfigCustomer.CONFIG_EVENT_STORE, "com.domosnap.customer.infra.eventstore.KafkaEventStore")
//            );
//		// Start server
//		vertx.deployVerticle(new ServerCustomer(), options, testContext.succeedingThenComplete());
//	}
//
//	/**
//	 * this test the API if it is working
//	 * @param context this is for Vertx unit test
//	 */
//	@Order(1)
//	@RepeatedTest(3)
//	@DisplayName("Test connection")
//	public void testConnection(VertxTestContext testContext) throws Throwable {
//		WebClient client = WebClient.create(vertx);
//
//		client
//		.get(port, host , "/customers")
//		.bearerTokenAuthentication(jwt)
//		.send(testContext.succeeding(ar -> testContext.verify(() -> {
//			assertThat(ar.statusCode()).isEqualTo(200);
//			testContext.completeNow();
//		})));
//	}
//
//
//	@Order(2)
//	@RepeatedTest(3)
//	@Timeout(value = 5, timeUnit = TimeUnit.SECONDS)
//	public void testCreateCustomer(VertxTestContext testContext) throws Throwable {
//		WebClient client = WebClient.create(vertx);
//
//		client
//			.post(port, host , "/customers")
//			.bearerTokenAuthentication(jwt)
//			.putHeader("idPartner", "1")
//			.sendJsonObject(new JsonObject()
//				.put("externalId", "test")
//				.put("contracts", Contracts), testContext.succeeding(ar -> testContext.verify(() -> {
//					assertThat(ar.statusCode()).isEqualTo(201);
//					String[] foundUid = ar.headers().get("Location").split("/");
//					assertThat(foundUid[foundUid.length - 1].length() == 36).isEqualTo(true);
//					assertThat(ar.bodyAsJsonObject().getString("id")).isEqualTo(foundUid[foundUid.length - 1]);
//					testContext.completeNow();
//				})));
//	}
//
//	@Order(3)
//	@RepeatedTest(3)
//	@Timeout(value = 5, timeUnit = TimeUnit.SECONDS)
//	public void testCreateWrongCustomer(VertxTestContext testContext) throws Throwable {
//		WebClient client = WebClient.create(vertx);
//		Checkpoint wrongCustomerCheckpoint = testContext.checkpoint(4);
//
//		// idPartner is missing
//		client
//			.post(port, host , "/customers")
//			.bearerTokenAuthentication(jwt)
//			.sendJsonObject(new JsonObject()
//				.put("externalId", "test")
//				.put("contracts", Contracts), testContext.succeeding(ar -> testContext.verify(() -> {
//					assertThat(ar.statusCode()).isEqualTo(400);
//					wrongCustomerCheckpoint.flag();
//				})));
//
//		// externalId is missing
//		client
//			.post(port, host , "/customers")
//			.bearerTokenAuthentication(jwt)
//			.putHeader("idPartner", "1")
//			.sendJsonObject(new JsonObject()
//				.put("contracts", Contracts), testContext.succeeding(ar -> testContext.verify(() -> {
//					assertThat(ar.statusCode()).isEqualTo(400);
//					wrongCustomerCheckpoint.flag();
//				})));
//
//		// contracts are missing
//		client
//			.post(port, host , "/customers")
//			.bearerTokenAuthentication(jwt)
//			.putHeader("idPartner", "1")
//			.sendJsonObject(new JsonObject()
//				.put("externalId", "test"), testContext.succeeding(ar -> testContext.verify(() -> {
//					assertThat(ar.statusCode()).isEqualTo(400);
//					wrongCustomerCheckpoint.flag();
//				})));
//
//		// jwt is missing
//		client
//			.post(port, host , "/customers")
//			.putHeader("idPartner", "1")
//			.sendJsonObject(new JsonObject()
//				.put("externalId", "test")
//				.put("contracts", Contracts), testContext.succeeding(ar -> testContext.verify(() -> {
//					assertThat(ar.statusCode()).isEqualTo(401);
//					wrongCustomerCheckpoint.flag();
//				})));
//	}
//
//	@Order(4)
//	@RepeatedTest(3)
//	@Timeout(value=5, timeUnit = TimeUnit.SECONDS)
//	public void testCreateUpdateWithId(VertxTestContext testContext) throws Throwable {
//		WebClient client = WebClient.create(vertx);
//
//		client
//			.put(port, host , "/customers/" + uid)
//			.bearerTokenAuthentication(jwt)
//			.putHeader("idPartner", "1")
//			.sendJsonObject(new JsonObject()
//				.put("externalId", "test")
//				.put("contracts", Contracts), testContext.succeeding(ar -> testContext.verify(() -> {
//					assertThat(ar.statusCode()).isEqualTo(201);
//					String[] foundUid = ar.headers().get("Location").split("/");
//					assertThat(foundUid[foundUid.length - 1]).isEqualTo(uid);
//					assertThat(ar.bodyAsJsonObject().getString("id")).isEqualTo(uid);
//
//					client
//						.put(port, host , "/customers/" + uid)
//						.bearerTokenAuthentication(jwt)
//						.putHeader("idPartner", "1")
//						.sendJsonObject(new JsonObject()
//							.put("externalId", "test")
//							.put("contracts", Contracts), testContext.succeeding(ar1 -> testContext.verify(() -> {
//								assertThat(ar1.statusCode()).isEqualTo(200);
//								assertThat(ar1.bodyAsJsonObject().getString("id")).isEqualTo(uid);
//								testContext.completeNow();
//							})));
//				})));
//	}
//
//	@Order(5)
//	@RepeatedTest(3)
//	@Timeout(value = 5, timeUnit = TimeUnit.SECONDS)
//	public void testUpdateWrongCustomer(VertxTestContext testContext) throws Throwable {
//		WebClient client = WebClient.create(vertx);
//		Checkpoint wrongCustomerCheckpoint = testContext.checkpoint(4);
//
//		// idPartner is missing
//		client
//			.put(port, host , "/customers/" + uid)
//			.bearerTokenAuthentication(jwt)
//			.sendJsonObject(new JsonObject()
//				.put("externalId", "test")
//				.put("contracts", Contracts), testContext.succeeding(ar -> testContext.verify(() -> {
//					assertThat(ar.statusCode()).isEqualTo(400);
//					wrongCustomerCheckpoint.flag();
//				})));
//
//		// externalId is missing
//		client
//			.put(port, host , "/customers/" + uid)
//			.bearerTokenAuthentication(jwt)
//			.putHeader("idPartner", "1")
//			.sendJsonObject(new JsonObject()
//				.put("contracts", Contracts), testContext.succeeding(ar -> testContext.verify(() -> {
//					assertThat(ar.statusCode()).isEqualTo(400);
//					wrongCustomerCheckpoint.flag();
//				})));
//
//		// contracts are missing
//		client
//			.put(port, host , "/customers/" + uid)
//			.bearerTokenAuthentication(jwt)
//			.putHeader("idPartner", "1")
//			.sendJsonObject(new JsonObject()
//				.put("externalId", "test"), testContext.succeeding(ar -> testContext.verify(() -> {
//					assertThat(ar.statusCode()).isEqualTo(400);
//					wrongCustomerCheckpoint.flag();
//				})));
//
//		// jwt is missing
//		client
//			.put(port, host , "/customers/" + uid)
//			.putHeader("idPartner", "1")
//			.sendJsonObject(new JsonObject()
//				.put("externalId", "test")
//				.put("contracts", Contracts), testContext.succeeding(ar -> testContext.verify(() -> {
//					assertThat(ar.statusCode()).isEqualTo(401);
//					wrongCustomerCheckpoint.flag();
//				})));
//	}
//
//	@Order(6)
//	@RepeatedTest(3)
//	public void testGetCustomer(VertxTestContext testContext) throws Throwable {
//		WebClient client = WebClient.create(vertx);
//		String getUid = UUID.randomUUID().toString();
//
//		client
//			.put(port, host , "/customers/" + getUid)
//			.bearerTokenAuthentication(jwt)
//			.putHeader("idPartner", "1")
//			.sendJsonObject(new JsonObject()
//				.put("externalId", "test")
//				.put("contracts", Contracts), testContext.succeeding(ar -> testContext.verify(() -> {
//					assertThat(ar.statusCode()).isEqualTo(201);
//
//					client
//						.get(port, host , "/customers/" + getUid)
//						.bearerTokenAuthentication(jwt)
//						.send(testContext.succeeding(ar1 -> testContext.verify(() -> {
//							assertThat(ar1.statusCode()).isEqualTo(200);
//							assertThat(ar1.bodyAsJsonObject().getString("id")).isEqualTo(getUid);
//							testContext.completeNow();
//						})));
//				})));
//
//
//	}
//
//	@Order(7)
//	@RepeatedTest(3)
//	public void testGetWrongCustomer(VertxTestContext testContext) throws Throwable {
//		WebClient client = WebClient.create(vertx);
//		Checkpoint getWrongCustomerCheckpoint = testContext.checkpoint(2);
//		String getUid = UUID.randomUUID().toString();
//
//		client
//			.post(port, host , "/customers")
//			.bearerTokenAuthentication(jwt)
//			.putHeader("idPartner", "1")
//			.sendJsonObject(new JsonObject()
//				.put("customerId", getUid)
//				.put("externalId", "test")
//				.put("contracts", Contracts), testContext.succeeding(ar -> testContext.verify(() -> {
//					assertThat(ar.statusCode()).isEqualTo(201);
//
//					client
//						.get(port, host , "/customers/" + getUid)
//						.send(testContext.succeeding(ar1 -> testContext.verify(() -> {
//							assertThat(ar1.statusCode()).isEqualTo(401);
//							getWrongCustomerCheckpoint.flag();
//						})));
//
//					client
//						.get(port, host , "/customers/" + uid)
//						.bearerTokenAuthentication(jwt)
//						.send(testContext.succeeding(ar2 -> testContext.verify(() -> {
//							assertThat(ar2.statusCode()).isEqualTo(404);
//							getWrongCustomerCheckpoint.flag();
//						})));
//
//				})));
//	}
//
//	@Order(8)
//	@RepeatedTest(3)
//	public void testDeleteCustomer(VertxTestContext testContext) throws Throwable {
//		WebClient client = WebClient.create(vertx);
//		String deleteUid = UUID.randomUUID().toString();
//
//		client
//			.put(port, host , "/customers/" + deleteUid)
//			.bearerTokenAuthentication(jwt)
//			.putHeader("idPartner", "1")
//			.sendJsonObject(new JsonObject()
//				.put("externalId", "test")
//				.put("contracts", Contracts), testContext.succeeding(ar -> testContext.verify(() -> {
//					assertThat(ar.statusCode()).isEqualTo(201);
//
//					client
//						.get(port, host , "/customers/" + deleteUid)
//						.bearerTokenAuthentication(jwt)
//						.send(testContext.succeeding(ar1 -> testContext.verify(() -> {
//							assertThat(ar1.statusCode()).isEqualTo(200);
//							assertThat(ar1.bodyAsJsonObject().getString("id")).isEqualTo(deleteUid);
//
//							client
//								.delete(port, host , "/customers/" + deleteUid)
//								.bearerTokenAuthentication(jwt)
//								.send(testContext.succeeding(ar2 -> testContext.verify(() -> {
//									assertThat(ar2.statusCode()).isEqualTo(204);
//
//									client
//										.get(port, host , "/customers/" + deleteUid)
//										.bearerTokenAuthentication(jwt)
//										.send(testContext.succeeding(ar3 -> testContext.verify(() -> {
//											assertThat(ar3.statusCode()).isEqualTo(404);
//											testContext.completeNow();
//										})));
//								})));
//						})));
//				})));
//	}
//
//	@Order(9)
//	@RepeatedTest(3)
//	public void testDeleteWrongCustomer(VertxTestContext testContext) throws Throwable {
//		WebClient client = WebClient.create(vertx);
//		Checkpoint deleteWrongCustomerCheckpoint = testContext.checkpoint(2);
//		String deleteUid = UUID.randomUUID().toString();
//
//		client
//			.put(port, host , "/customers/" + deleteUid)
//			.bearerTokenAuthentication(jwt)
//			.putHeader("idPartner", "1")
//			.sendJsonObject(new JsonObject()
//				.put("externalId", "test")
//				.put("contracts", Contracts), testContext.succeeding(ar -> testContext.verify(() -> {
//					assertThat(ar.statusCode()).isEqualTo(201);
//
//					client
//						.get(port, host , "/customers/" + deleteUid)
//						.bearerTokenAuthentication(jwt)
//						.send(testContext.succeeding(ar1 -> testContext.verify(() -> {
//							assertThat(ar1.statusCode()).isEqualTo(200);
//							assertThat(ar1.bodyAsJsonObject().getString("id")).isEqualTo(deleteUid);
//
//							client
//								.delete(port, host , "/customers/" + deleteUid)
//								.send(testContext.succeeding(ar2 -> testContext.verify(() -> {
//									assertThat(ar2.statusCode()).isEqualTo(401);
//									deleteWrongCustomerCheckpoint.flag();
//								})));
//
//							client
//								.get(port, host , "/customers/" + uid)
//								.bearerTokenAuthentication(jwt)
//								.send(testContext.succeeding(ar2 -> testContext.verify(() -> {
//									assertThat(ar2.statusCode()).isEqualTo(404);
//									deleteWrongCustomerCheckpoint.flag();
//								})));
//						})));
//				})));
//	}
//
//	@Order(10)
//	@RepeatedTest(3)
//	public void testGetCustomers(VertxTestContext testContext) throws Throwable {
//		WebClient client = WebClient.create(vertx);
//		Checkpoint getCustomersCheckpoint = testContext.checkpoint(6);
//
//		client
//			.put(port, host , "/customers/" + uid)
//			.bearerTokenAuthentication(jwt)
//			.putHeader("idPartner", "1")
//			.sendJsonObject(new JsonObject()
//				.put("externalId", "id1")
//				.put("contracts", Contracts1), testContext.succeeding(ar -> testContext.verify(() -> {
//					assertThat(ar.statusCode()).isEqualTo(201);
//
//					client
//						.put(port, host , "/customers/" + uid1)
//						.bearerTokenAuthentication(jwt)
//						.putHeader("idPartner", "1")
//						.sendJsonObject(new JsonObject()
//							.put("externalId", "id2")
//							.put("contracts", Contracts), testContext.succeeding(ar1 -> testContext.verify(() -> {
//								assertThat(ar1.statusCode()).isEqualTo(201);
//
//								client
//									.get(port, host , "/customers")
//									.bearerTokenAuthentication(jwt)
//									.send(testContext.succeeding(ar2 -> testContext.verify(() -> {
//										assertThat(ar2.statusCode()).isEqualTo(200);
//										assertThat(ar2.bodyAsJsonArray().size() >= 2).isEqualTo(true);
//										getCustomersCheckpoint.flag();
//									})));
//
//								client
//									.get(port, host , "/customers?external_id=id1")
//									.bearerTokenAuthentication(jwt)
//									.send(testContext.succeeding(ar2 -> testContext.verify(() -> {
//										assertThat(ar2.statusCode()).isEqualTo(200);
//										JsonArray result = ar2.bodyAsJsonArray();
//										assertThat(result.size()).isEqualTo(1);
//										JsonObject customer = new JsonObject(result.getString(0));
//										assertThat(customer.getString("id")).isEqualTo(uid);
//										getCustomersCheckpoint.flag();
//									})));
//
//								client
//									.get(port, host , "/customers?contract_id=" + Contract3.getString("idContract"))
//									.bearerTokenAuthentication(jwt)
//									.send(testContext.succeeding(ar2 -> testContext.verify(() -> {
//										assertThat(ar2.statusCode()).isEqualTo(200);
//										JsonArray result = ar2.bodyAsJsonArray();
//										assertThat(result.size()).isEqualTo(1);
//										JsonObject customer = new JsonObject(result.getString(0));
//										assertThat(customer.getString("id")).isEqualTo(uid);
//										getCustomersCheckpoint.flag();
//									})));
//
//								client
//									.get(port, host , "/customers?external_id=id1&contract_id=" + Contract3.getString("idContract"))
//									.bearerTokenAuthentication(jwt)
//									.send(testContext.succeeding(ar2 -> testContext.verify(() -> {
//										assertThat(ar2.statusCode()).isEqualTo(200);
//										JsonArray result = ar2.bodyAsJsonArray();
//										assertThat(result.size()).isEqualTo(1);
//										JsonObject customer = new JsonObject(result.getString(0));
//										assertThat(customer.getString("id")).isEqualTo(uid);
//										getCustomersCheckpoint.flag();
//									})));
//
//								client
//									.get(port, host , "/customers?external_id=" + UUID.randomUUID().toString())
//									.bearerTokenAuthentication(jwt)
//									.send(testContext.succeeding(ar2 -> testContext.verify(() -> {
//										assertThat(ar2.statusCode()).isEqualTo(200);
//										assertThat(ar2.bodyAsJsonArray().size()).isEqualTo(0);
//										getCustomersCheckpoint.flag();
//									})));
//
//								client
//									.get(port, host , "/customers?contract_id=" + UUID.randomUUID().toString())
//									.bearerTokenAuthentication(jwt)
//									.send(testContext.succeeding(ar2 -> testContext.verify(() -> {
//										assertThat(ar2.statusCode()).isEqualTo(200);
//										assertThat(ar2.bodyAsJsonArray().size()).isEqualTo(0);
//										getCustomersCheckpoint.flag();
//									})));
//
//							})));
//				})));
//	}
//
//    @AfterAll
//    public static void close() {
//        kafkaSimulator.destroy();
//    }
//}