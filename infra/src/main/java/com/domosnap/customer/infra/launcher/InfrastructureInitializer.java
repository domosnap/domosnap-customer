package com.domosnap.customer.infra.launcher;


import com.domosnap.customer.infra.port.driven.repositories.CustomerRepository;
import com.domosnap.customer.infra.port.driven.repositories.DirectoryRepository;
import com.domosnap.customer.domain.directory.Directory;
import com.domosnap.customer.infra.handlers.UpdateDirectoryHandler;

import com.domosnap.tools.core.services.cqrs.domain.EventPublisher;
import com.domosnap.tools.core.services.cqrs.infra.handler.DispatchEventPublisher;
import com.domosnap.tools.core.services.resourcenames.ResourceName;
import com.domosnap.tools.core.services.cqrs.infra.eventStore.EventStore;
import com.domosnap.tools.core.services.cqrs.infra.handler.PersistingEventPublisher;
import com.domosnap.customer.infra.eventstore.KafkaEventStore;
import com.domosnap.tools.core.services.cqrs.infra.eventStore.impl.InMemoryEventStore;

import jakarta.enterprise.context.ApplicationScoped;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import java.util.concurrent.CompletableFuture;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

@ApplicationScoped
public class InfrastructureInitializer {

    private static final Logger log = Logger.getLogger(InfrastructureInitializer.class.getName());
    private final String kafkaServers;
    private final String kafkaTopic;
    private final String kafkaTopicRepo;
    private final String eventStoreClass;

    private CustomerRepository customerRepository;
    private DirectoryRepository directoryRepository;
    private EventPublisher eventPublisher;

    public InfrastructureInitializer(
            @ConfigProperty(name = "kafka.bootstrap.servers") String kafkaServers,
            @ConfigProperty(name = "kafka.topic.main") String kafkaTopic,
            @ConfigProperty(name = "kafka.topic.repo") String kafkaTopicRepo,
            @ConfigProperty(name = "event.store.class") String eventStoreClass
    ) {
        this.kafkaServers = kafkaServers;
        this.kafkaTopic = kafkaTopic;
        this.kafkaTopicRepo = kafkaTopicRepo;
        this.eventStoreClass = eventStoreClass;
        createInfra().join();
    }

    private CompletableFuture<Void> createInfra() {
        return CompletableFuture.runAsync(() -> {
            DispatchEventPublisher smartDeviceEventDispatcher = new DispatchEventPublisher();
            this.customerRepository = createCustomerRepository(eventStoreClass, smartDeviceEventDispatcher);

            DispatchEventPublisher directoryEventDispatcher = new DispatchEventPublisher();
            this.directoryRepository = createDirectoryRepository(eventStoreClass, directoryEventDispatcher);

            UpdateDirectoryHandler udh = new UpdateDirectoryHandler(directoryRepository, directoryEventDispatcher);
            smartDeviceEventDispatcher.register(udh);
            eventPublisher = smartDeviceEventDispatcher;
        });
    }

    private EventStore createEventStore(Map<String, Object> config) {
        EventStore store;
        if (eventStoreClass.equals("com.domosnap.tools.core.services.cqrs.infra.eventStore.impl.InMemoryEventStore")) {
            store = new InMemoryEventStore();
        } else if (eventStoreClass.equals("com.domosnap.customer.infra.eventstore.KafkaEventStore")) {
            store = new KafkaEventStore();
        } else {
            throw new IllegalArgumentException("EventStore type not supported: " + eventStoreClass);
        }
        store.init(config);
        return store;
    }

    private CustomerRepository createCustomerRepository( String eventStoreClass, DispatchEventPublisher eventPublisher) {
        Map<String, Object> esConfiguration = new HashMap<>();

        esConfiguration.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaServers);
        esConfiguration.put(KafkaEventStore.TOPIC_CONFIG, kafkaTopic);
        esConfiguration.put(ConsumerConfig.GROUP_ID_CONFIG, "customer-group");

        EventStore eventS = createEventStore(esConfiguration);
        eventS.init(esConfiguration);
        // Read
        CustomerRepository sr = new CustomerRepository(eventS);
        // Write
        eventPublisher.register(new PersistingEventPublisher(eventS));
        return sr;

    }

    private DirectoryRepository createDirectoryRepository(String eventStoreClass, DispatchEventPublisher eventPublisher) {
        Map<String, Object> esConfiguration = new HashMap<>();

        esConfiguration.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaServers);
        esConfiguration.put(KafkaEventStore.TOPIC_CONFIG, kafkaTopicRepo);
        esConfiguration.put(ConsumerConfig.GROUP_ID_CONFIG, "directory-group-customer");

        EventStore eventStore = createEventStore(esConfiguration);
        eventStore.init(esConfiguration);
        // Read: repo make able to get aggregat directory
        DirectoryRepository dr = new DirectoryRepository(eventStore);
        // Write
        eventPublisher.register(new PersistingEventPublisher(eventStore));
        Directory.create(new ResourceName("Profile", "directory", "system"), eventPublisher); // TODO ici on cré toujours le directory...
        //	try {
        //		dr.getById(Directory.mainDirectory.getId()); // TODO ici on créé le directory si il n'existe pas...
        //	} catch(NotFoundException e) {
        //		Directory.create(new ResourceName("Profile", "directory", "system"), directoryEventDispatcher);
        //		log.info("Directory created");
        //	}
        return dr;
    }

    public CustomerRepository createCustomerRepository() {
        return customerRepository;
    }

    public DirectoryRepository getDirectoryRepository() {
        return directoryRepository;
    }

    public EventPublisher getEventPublisher() {
        return eventPublisher;
    }

}
