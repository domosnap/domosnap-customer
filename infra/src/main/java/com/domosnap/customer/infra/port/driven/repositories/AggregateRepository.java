package com.domosnap.customer.infra.port.driven.repositories;

import java.util.List;
import java.util.NoSuchElementException;

import com.domosnap.tools.core.services.cqrs.domain.Event;
import com.domosnap.tools.core.services.cqrs.infra.eventStore.EventStore;

public abstract class AggregateRepository<T> {
    protected EventStore eventStore;

    public AggregateRepository(EventStore eventStore) {
        this.eventStore = eventStore;
    }

    // TODO transformer String en AggregatedId?
    public T getById(String aggregateId) {
    	if (aggregateId == null) {
    		return null;
    	}
    	
        List<Event<?>> history = eventStore.getEventsOfAggregate(aggregateId);
        if (history == null || history.isEmpty()) {
            throw new NoSuchElementException("" + this.getClass().getName() + " : " + aggregateId);
        } else {
            return fromHistory(history);
        }
    }

    protected abstract T fromHistory(List<Event<?>> history);
}
