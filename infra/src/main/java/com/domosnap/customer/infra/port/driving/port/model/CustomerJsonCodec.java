/* CustomerJsonCodec
 * Transform a customer into JSON
 * Raphaël Lopes
 * 06.06.2020
 */
package com.domosnap.customer.infra.port.driving.port.model;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import com.domosnap.customer.domain.user.Contract;
import com.domosnap.customer.domain.user.CustomerId;
import com.domosnap.customer.domain.user.CustomerProjection;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class CustomerJsonCodec {
	private CustomerJsonCodec()
	{
		
	}
	/**
	 * transform a customer into JSON
	 * @param customer the customer
	 * @return JSON of the customer
	 */
	public static final String toJson(CustomerProjection customer) {
		if (customer == null) {
			return "";
		}
	    StringBuilder sb = new StringBuilder();
	    sb.append("{\"id\":\"").append(customer.id).append("\",")
	      .append("\"idPartner\":").append(customer.idPartner).append(",")
	      .append("\"externalId\":\"").append(customer.externalId).append("\",")
	      .append("\"contracts\":").append(ContractJsonCodec.ContractListToJson(customer)).append(",")
	      .append("\"deleted\":").append(customer.deleted).append("}");
	
	    return sb.toString();
	}

	public static final CustomerProjection fromJson(String json) {
		if (json == null) {
			return null;
		}
		try {
			JsonObject jo = new JsonObject(json);
			String idCustomer = jo.getString("id");
			Integer idPartner = jo.getInteger("idPartner");
			String externalId = jo.getString("externalId");
			Boolean deleted = jo.getBoolean("deleted");
			JsonArray contractsArray = jo.getJsonArray("contracts");
			List<Contract> contracts = new ArrayList<Contract>();
		
			contractsArray.forEach(contractObjectString -> {
				if (contractObjectString instanceof JsonObject) {
					JsonObject contractJsonString = (JsonObject) contractObjectString;
					Optional<Contract> newContract = ContractJsonCodec.fromJson(contractJsonString.toString());
					if(newContract.isPresent()) {
						contracts.add(newContract.get());
					}
				}
			});
			CustomerProjection c = new CustomerProjection(new CustomerId(idCustomer), idPartner, externalId, contracts, deleted);
			System.out.println(c);
			return c;
		}
		catch(NoSuchElementException exception) {
			exception.printStackTrace();
			return null;
		}
		catch (NullPointerException e3) {
			e3.printStackTrace();
			return null;
		}
		catch (ClassCastException e4) {
			e4.printStackTrace();
			return null;
		}
	}
}