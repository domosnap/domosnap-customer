package com.domosnap.customer.infra.port.driven.repositories;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import com.domosnap.customer.domain.directory.Directory;
import com.domosnap.tools.core.services.cqrs.domain.Event;
import com.domosnap.tools.core.services.cqrs.infra.eventStore.EventStore;
import com.domosnap.tools.core.services.cqrs.infra.eventStore.EventStoreFactory;
import com.domosnap.tools.core.services.cqrs.infra.handler.DispatchEventPublisher;
import com.domosnap.tools.core.services.cqrs.infra.handler.PersistingEventPublisher;
import com.domosnap.tools.core.services.eventstore.MongoDBEventStore;
import com.domosnap.tools.core.services.resourcenames.ResourceName;

public class DirectoryRepository extends AsyncAggregateRepository<Directory> {
    
	
    public DirectoryRepository(EventStore eventStore) {
        super(eventStore);
    }

    @Override
    protected Directory fromHistory(List<Event<?>> history) {
        return new Directory(history);
    }
    
    public  CompletableFuture<Directory> getDirectory() {
    	return getById(Directory.mainDirectory.getId());
    }


    public static DirectoryRepository createInfra(String mongoDBUrl, String eventStoreClass, DispatchEventPublisher eventPublisher) {
        Map<String, Object> esConfiguration = new HashMap<>();
        esConfiguration.put(MongoDBEventStore.AGGREGATE_TYPE, "directory");
        esConfiguration.put(MongoDBEventStore.MONGO_URL, mongoDBUrl);

        EventStore eventStore = EventStoreFactory.getEventStore(eventStoreClass, esConfiguration);

        // Read: repo make able to get aggregat directory
        DirectoryRepository dr = new DirectoryRepository(eventStore);
        // Write
        eventPublisher.register(new PersistingEventPublisher(eventStore)); // Add persistent handler

        Directory.create(new ResourceName("Profile", "directory", "system"), eventPublisher); // TODO ici on cré toujours le directory...
        //	try {
        //		dr.getById(Directory.mainDirectory.getId()); // TODO ici on créé le directory si il n'existe pas...
        //	} catch(NotFoundException e) {
        //		Directory.create(new ResourceName("Profile", "directory", "system"), directoryEventDispatcher);
        //		log.info("Directory created");
        //	}

        return dr;
    }

}
