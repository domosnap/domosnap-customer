/* CustomerRepository
 * this is the repository of the customer
 * Jossef Roncal
 * 10.05.2021
 */
package com.domosnap.customer.infra.port.driven.repositories;


import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

import com.domosnap.tools.core.services.cqrs.domain.Event;
import com.domosnap.tools.core.services.cqrs.infra.eventStore.EventStore;


public abstract class AsyncAggregateRepository<T> {
    protected ProxyAggregateRepository aggregateRepository;
    protected EventStore eventStore;

    /**
	 * Construct of a AsyncAggregateRepository
	 * @param eventStore A EventStore
	 */
    public AsyncAggregateRepository(EventStore eventStore) {
        this.eventStore = eventStore;
        this.aggregateRepository = new ProxyAggregateRepository(eventStore);
    }

    /**
	 * Get a aggregation as a future from its id
	 * @param aggregateId Id of the aggregation
	 */
    public CompletableFuture<T> getById(String aggregateId) {
        return CompletableFuture.supplyAsync(() -> this.aggregateRepository.getById(aggregateId));
    }

    protected CompletableFuture<T> asyncFromHistory(List<Event<?>> history) {
        return CompletableFuture.supplyAsync(() -> this.fromHistory(history));
    }

    protected abstract T fromHistory(List<Event<?>> history);

    /**
	 * Accept a consumer of T if the aggregation is present in an optional
	 * @param consumer Consumer to be accepted
	 */
    public void ifPresent(String aggregateId, Consumer<T> consumer) {
        this.getById(aggregateId).thenAccept(consumer);
    }

    /**
	 * Accept a consumer of T if the aggregation is present in an optional
	 * @param consumer Consumer to be accepted
	 */
    public void ifCreated(String aggregateId, Consumer<T> consumer) {
        this.ifPresent(aggregateId, consumer);
    }

    public void ifPresentOrElse(String aggregateId, Consumer<T> consumerPresent, Runnable runnableAbsent) {
        this.getById(aggregateId).thenAccept(consumerPresent).exceptionally(ex -> { 
            runnableAbsent.run(); 
            return null;
        });
    }

    public void ifAbsent(String aggregateId, Runnable runnableAbsent) {
        this.getById(aggregateId).exceptionally(ex -> { 
            runnableAbsent.run(); 
            return null;
        });
    }

    private class ProxyAggregateRepository extends AggregateRepository<T> {
        
        public ProxyAggregateRepository(EventStore eventStore) {
            super(eventStore);
        }

        @Override
        protected T fromHistory(List<Event<?>> history) {
            return AsyncAggregateRepository.this.fromHistory(history);
        }

        public T getById(String aggregateId) {
            if (aggregateId == null) {
                return null;
            }
            
            List<Event<?>> history = eventStore.getEventsOfAggregate(aggregateId); // changed aggregateId by getTopic
            if (history == null || history.isEmpty()) {
                throw new NoSuchElementException("" + this.getClass().getName() + " : " + aggregateId);
            } else {
                return fromHistory(history);
            }
        }

    }
}

