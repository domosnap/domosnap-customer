/* CustomerRepository
 * this is the repository of the customer
 * Raphaël Lopes
 * 06.06.2020
 */
package com.domosnap.customer.infra.port.driven.repositories;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import com.domosnap.customer.domain.user.Customer;
import com.domosnap.customer.domain.user.CustomerId;
import com.domosnap.tools.core.services.cqrs.domain.Event;
import com.domosnap.tools.core.services.cqrs.infra.eventStore.EventStore;
import com.domosnap.tools.core.services.cqrs.infra.eventStore.EventStoreFactory;
import com.domosnap.tools.core.services.cqrs.infra.handler.DispatchEventPublisher;
import com.domosnap.tools.core.services.cqrs.infra.handler.PersistingEventPublisher;
import com.domosnap.tools.core.services.resourcenames.ResourceName;
import com.domosnap.tools.core.services.eventstore.MongoDBEventStore;


public class CustomerRepository extends AsyncAggregateRepository<Customer> {

    private static EventStore eventS;

    public CustomerRepository(EventStore eventStore) {
        super(eventStore);
    }

    @Override
    protected Customer fromHistory(List<Event<?>> history) {
        return new Customer(history);
    }  

    /**
	 * Create a customer (aggregation) as a future and put it in an optional for later use.
	 */
    public CompletableFuture<Customer> create(CustomerId customerId, Integer idPartner, String externalId, ResourceName principal, Consumer<Event<?>> eventPublisher) {
        CompletableFuture<Void> creation = CompletableFuture.runAsync(() -> {
            Customer.create(customerId, idPartner, externalId, principal, eventPublisher);
        });

        return creation.thenCompose(n -> this.getById(customerId.getId()));
    }

    /**
     * Returns the list of customer from a list of customer id.
     * @param customers {@link CustomerId} list.
     * @return List of {@link Customer}
     */
    public List<CompletableFuture<Customer>> getCustomers(List<CustomerId> customers) {
        return customers.stream()
			.map(customerId -> {
				return this.getById(customerId.getId());
			}).collect(Collectors.toList());
    }



    public static CustomerRepository createInfra(String mongoDBUrl, String eventStoreClass, DispatchEventPublisher eventPublisher) {
        Map<String, Object> esConfiguration = new HashMap<>();
        esConfiguration.put(MongoDBEventStore.AGGREGATE_TYPE, "customers");
        esConfiguration.put(MongoDBEventStore.MONGO_URL, mongoDBUrl);
        eventS = EventStoreFactory.getEventStore(eventStoreClass, esConfiguration);

        // Read
        CustomerRepository sr = new CustomerRepository(eventS);
        // Write
        eventPublisher.register(new PersistingEventPublisher(eventS)); // Add persistent handler


        return sr;
    }

}
