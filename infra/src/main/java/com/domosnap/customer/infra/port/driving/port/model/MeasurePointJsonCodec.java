/* MeasurePointJsonCodec
 * Transform a measure point into JSON
 * Raphaël Lopes
 * 06.06.2020
 */
package com.domosnap.customer.infra.port.driving.port.model;

import com.domosnap.customer.domain.user.MeasurePoint;
import com.domosnap.customer.domain.user.Register;

public class MeasurePointJsonCodec {
	private MeasurePointJsonCodec()
	{
		
	}
	/**
	 * transform a measurePoint to JSON
	 * @param measurePoint the measure point
	 * @return JSON of the measure point
	 */
	public static final String toJson(MeasurePoint measurePoint) {
		if (measurePoint == null) {
			return "";
		}
	    StringBuilder sb = new StringBuilder();
	    sb.append("{\"idPartner\":").append(measurePoint.getIdPartner()).append(",")
			.append("\"idContract\":\"").append(measurePoint.getIdContract()).append("\",")
			.append("\"registers\":").append("[");
		for (Register register : measurePoint.getRegister()) {
			sb.append("{\"idRegister\":\"").append(register.getIdRegister()).append("\","); 
			sb.append("\"idPartner\":").append(register.getIdPartner()).append(","); 
			sb.append("\"uri\":\"").append(register.getUri()).append("\"},"); 
		}
		if (measurePoint.getRegister().size() > 0) {
			sb.setLength(sb.length()-1);
		}
		sb.append("]}");
	    return sb.toString();
	}
	/**
	 * transform a register to a JSON
	 * @param register the register
	 * @return JSON of the register
	 */
	public static final String RegisterToJson(Register register) {
		if (register == null) {
			return "";
		}
	    StringBuilder sb = new StringBuilder();
	    	  sb.append("{\"idRegister\":\"").append(register.getIdRegister()).append("\","); 
	    	  sb.append("\"idPartner\":").append(register.getIdPartner()).append(","); 
	    	  sb.append("\"uri\":\"").append(register.getUri()).append("\"}"); 
	    return sb.toString();
	}
}
