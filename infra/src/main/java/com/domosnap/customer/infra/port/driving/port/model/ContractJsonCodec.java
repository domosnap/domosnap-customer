/* ContractJsonCodec
 * transform a contract into JSON
 * Raphaël Lopes
 * 06.06.2020
 */
package com.domosnap.customer.infra.port.driving.port.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import com.domosnap.customer.domain.user.Contract;
import com.domosnap.customer.domain.user.CustomerProjection;
import com.domosnap.customer.domain.user.MeasurePoint;
import com.domosnap.customer.domain.user.Register;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
public class ContractJsonCodec {
	private ContractJsonCodec()
	{
		
	}
	/**
	 * take a customer and return every contract in a JSON
	 * @param customer the customer
	 * @return JSON contract list
	 */
	public static final String ContractListToJson(CustomerProjection customer) {
		return getList(customer.contractList);
	}
	/**
	 * Take a contract and transform it to JSON
	 * @param contract the contract
	 * @return JSON contract
	 */
	public static final String toJson(Contract contract) {
		StringBuilder sb = new StringBuilder();
		sb.append("{\"idPartner\":").append(contract.getIdPartner().toString()).append(",")
			.append("\"idContract\":\"").append(contract.getIdContract()).append("\",")
			.append("\"idCustomer\":\"").append(contract.getIdCustomer()).append("\",")
			.append("\"type\":\"").append(contract.getType()).append("\",")
			.append("\"measuresPoints\":").append("[");
			for (MeasurePoint measurePoint : contract.getMeasuresPoints()) {
				sb.append(MeasurePointJsonCodec.toJson(measurePoint)).append(",");
			}
	      	if (contract.getMeasuresPoints().size() > 0) {
				sb.setLength(sb.length()-1);
			}
	      	sb.append("],")
	      	.append("\"beginDate\":\"").append(contract.getBeginDate().toString()).append("\",");
		if(contract.getEndDate().isPresent()) {
			sb.append("\"endDate\":\"").append(contract.getEndDate().get().toString()).append("\"}");
		} else {
			sb.append("\"endDate\":\"").append("null").append("\"}");
		}
	      
		return sb.toString();
	}
	/**
	 * Get a list of contract and transform into JSON
	 * @param contractList the list of contract
	 * @return JSON of the contract list
	 */
	public static final String getList(List<Contract> contractList) {
		StringBuilder sb = new StringBuilder("[");
		for (Contract contract : contractList) {
			sb.append(ContractJsonCodec.toJson(contract));
			sb.append(",");
		} 
		if (contractList.size() > 0) {
			sb.setLength(sb.length()-1);
		}
		
		sb.append("]");
		return sb.toString();
	}	
	/**
	 * transform a JSON into a contract
	 * @param json JSON
	 * @return a contract
	 */
	public static final Optional<Contract> fromJson(String json) {
		if (json == null) {
			return null;
		}
		JsonObject jo = new JsonObject(json);
		
		String idContract = jo.getString("idContract");
		Integer idPartner = jo.getInteger("idPartner");
		JsonArray measuresPointsArray = jo.getJsonArray("measuresPoints");
		List<MeasurePoint> measuresPoints = new ArrayList<MeasurePoint>();
		try {
			measuresPointsArray.forEach(registerObjectString -> {
				if (registerObjectString instanceof JsonObject) {
					JsonObject registerJsonObject = (JsonObject) registerObjectString;
					List<Register> register = new ArrayList<Register>();
					registerJsonObject.getJsonArray("registers").forEach(registerString -> {
						if (registerString instanceof JsonObject) {
							register.add(new Register(idPartner,((JsonObject)registerString).getString("idRegister"),((JsonObject)registerString).getString("uri")));
						} else {
							//TODO:add
							throw new NoSuchElementException();
						}
					});
					measuresPoints.add(new MeasurePoint(idPartner, idContract, register));
				} else {
					throw new NoSuchElementException();
				}
			});
			Long endDate = jo.getLong("endDate");
			Optional<String> type = Optional.of(jo.getString("type"));
			if(!type.isPresent()) {
				throw new NoSuchElementException();
			}
			if(endDate != null) {
				return Optional.of(new Contract(idPartner, idContract, jo.getString("idCustomer"), type.get(), measuresPoints, new Date(jo.getLong("beginDate")), new Date(endDate)));
			} else {
				return Optional.of(new Contract(idPartner, idContract, jo.getString("idCustomer"), type.get(), measuresPoints, new Date(jo.getLong("beginDate"))));
			}
		}
		catch(NoSuchElementException exception) {
			return Optional.empty();
		}
		catch (NullPointerException e3) {
			return Optional.empty();
		}
		catch (ClassCastException e4) {
			return Optional.empty();
		}
	}
}
