package com.domosnap.customer.infra.port.driving.port;


import com.domosnap.customer.infra.launcher.InfrastructureInitializer;
import com.domosnap.customer.infra.port.driven.repositories.CustomerRepository;
import com.domosnap.customer.infra.port.driven.repositories.DirectoryRepository;
import com.domosnap.customer.infra.port.driving.port.model.ContractJsonCodec;
import com.domosnap.customer.infra.port.driving.port.model.CustomerJsonCodec;
import com.domosnap.customer.infra.port.driving.port.model.MeasurePointJsonCodec;
import com.domosnap.tools.core.services.cqrs.domain.EventPublisher;
import com.domosnap.tools.core.services.resourcenames.ResourceName;
import io.quarkus.runtime.Quarkus;
import io.quarkus.security.Authenticated;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import com.domosnap.customer.domain.user.Contract;
import com.domosnap.customer.domain.user.Customer;
import com.domosnap.customer.domain.user.CustomerId;
import com.domosnap.customer.domain.user.MeasurePoint;
import com.domosnap.customer.domain.user.Register;
import jakarta.ws.rs.core.*;
import io.quarkus.vertx.web.Route;


@Authenticated
public class CustomerQuarkusRestHandler {

    private static final String USER_ID_STRING = "username";
    private static final String ID_PARTNER_STRING = "idPartner";
    private static final String CUSTOMER_ID_STRING = "customerId";
    private static  final String ID_CONTRACT_STRING= "idContract";
    private static  final String ID_REGISTER_STRING = "idRegister";
    private static final String CUSTOMERS_PATH = "Customers";
    private static final String CONTENT_TYPE = "Content-Type";
    private static final String APP_CHARSET = "application/json;charset=UTF-8";
    private Logger log = Logger.getLogger(CustomerQuarkusRestHandler.class.getName());
    private final CustomerRepository customerRepository;
    private final DirectoryRepository directoryRepository;
    private final EventPublisher eventPublisher;



    public CustomerQuarkusRestHandler(InfrastructureInitializer infra) {
        this.customerRepository = infra.createCustomerRepository();
        this.directoryRepository = infra.getDirectoryRepository();
        this.eventPublisher = infra.getEventPublisher();
    }

    public static void main(String[] var0) {
        Quarkus.run(var0);
    }


	private void createCustomerFromData(RoutingContext rc, String customerId, String creatorId, Integer idPartner, String externalId, JsonArray contractArray) {
		CustomerId uid = new CustomerId(customerId);
		ResourceName creator = new ResourceName(ResourceName.DOMAIN_PROFILE, CUSTOMERS_PATH, creatorId);
		customerRepository.create(uid, idPartner, externalId, creator, eventPublisher)
			.thenAccept(customer -> {
                contractArray.forEach(contractObject -> {
					if (contractObject instanceof JsonObject) {
						JsonObject contractJson = (JsonObject)contractObject;
						contractJson.put(ID_PARTNER_STRING,idPartner).put(CUSTOMER_ID_STRING, customerId);
						Contract contract = ContractJsonCodec.fromJson(contractJson.toString()).get();
						if(contract.getEndDate().isPresent()) {
							customer.createContract(idPartner, contract.getIdContract(), customerId,contract.getType() ,contract.getMeasuresPoints(), contract.getBeginDate(), contract.getEndDate().get(), creator, eventPublisher);
						} else {
							customer.createContract(idPartner, contract.getIdContract(), customerId,contract.getType() ,contract.getMeasuresPoints(), contract.getBeginDate(), creator, eventPublisher);
						}
					} else {
						throw new NoSuchElementException();
					}
				});
				rc.response()
					.putHeader("Location", rc.normalizedPath() + "/" + customerId)
					.putHeader(CONTENT_TYPE, APP_CHARSET)
					.setStatusCode(201)
					.end(CustomerJsonCodec.toJson(customer.getCustomerProjection()));
			})
			.exceptionally(ex1 -> {
				rc.response().setStatusCode(400).end();
				return null;
			});
	}


    /**
     * create a customer
     * @param rc routing context
     */
    @Route(path = "/customers", methods = Route.HttpMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON , order = 2)
    public void createCustomer(RoutingContext rc) {
        try {
            Optional<String> mayBeCreatorId = Optional.ofNullable(rc.user().principal().getString(USER_ID_STRING));

            //Get customer to create
            JsonObject body = rc.getBodyAsJson();

            String customerId = UUID.randomUUID().toString();

            Optional<Integer> mayBeIdPartner = Optional.of(0);
            Optional<JsonArray> mayBeContractArray = Optional.ofNullable(body.getJsonArray("contracts"));
            Optional<String> mayBeExternalId = Optional.ofNullable(body.getString("externalId"));

            if(mayBeCreatorId.isPresent() && mayBeIdPartner.isPresent() && mayBeContractArray.isPresent() && mayBeExternalId.isPresent()) {
                customerRepository.ifPresentOrElse(
                        customerId,
                        customer -> rc.response().setStatusCode(409).end(),
                        () -> this.createCustomerFromData(rc, customerId, mayBeCreatorId.get(), mayBeIdPartner.get(), mayBeExternalId.get(), mayBeContractArray.get())
                );
            } else {
                rc.response().setStatusCode(400).end();
            }
        }
        catch (NumberFormatException e2) {
            // Exception thrown by parseInt
            log.severe(e2.getMessage());
            rc.response().setStatusCode(400).end();
        }
    }

    /**
     * create or update a customer
     * @param rc routing context
     */
    @Route(path = "/customers/:customerId", methods = Route.HttpMethod.PUT, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON, order = 3)
    public void createOrUpdateCustomer(RoutingContext rc) {
        try {
            Optional<String> mayBeCreatorId = Optional.ofNullable(rc.user().principal().getString(USER_ID_STRING));

            //Get customer to create
            JsonObject body = rc.getBodyAsJson();

            Optional<String> mayBeCustomerId = Optional.ofNullable(rc.pathParam(CUSTOMER_ID_STRING));

            Optional<Integer> mayBeIdPartner = Optional.of(0);
            Optional<JsonArray> mayBeContractArray = Optional.ofNullable(body.getJsonArray("contracts"));
            Optional<String> mayBeExternalId = Optional.ofNullable(body.getString("externalId"));

            if(mayBeCreatorId.isPresent() && mayBeIdPartner.isPresent() && mayBeContractArray.isPresent() && mayBeExternalId.isPresent() && mayBeCustomerId.isPresent()) {
                customerRepository.ifPresentOrElse(
                        mayBeCustomerId.get(),
                        customer -> rc.response().putHeader(CONTENT_TYPE, APP_CHARSET).setStatusCode(200).end(CustomerJsonCodec.toJson(customer.getCustomerProjection())),
                        () -> this.createCustomerFromData(rc, mayBeCustomerId.get(), mayBeCreatorId.get(), mayBeIdPartner.get(), mayBeExternalId.get(), mayBeContractArray.get())
                );
            } else {
                rc.response().setStatusCode(400).end();
            }
        }
        catch (NumberFormatException e2) {
            // Exception thrown by parseInt
            log.severe(e2.getMessage());
            rc.response().setStatusCode(400).end();
        }
    }

    /**
     * get a contract with an ID
     * @param rc routing context
     */
    @Route(path = "/customers/:customerId/contract/:Idcontract", methods = Route.HttpMethod.GET, produces = MediaType.APPLICATION_JSON, order = 5)
    public void getContract(RoutingContext rc) {
        try {
            Optional<String> mayBeCustomerId = Optional.of(rc.pathParam(CUSTOMER_ID_STRING));
            Optional<Integer> mayBeIdPartner = Optional.of(0);
            Optional<String> mayBeIdContract = Optional.of(rc.pathParam(ID_CONTRACT_STRING));

            if(mayBeCustomerId.isPresent() && mayBeIdContract.isPresent() && mayBeIdContract.isPresent()) {
                customerRepository.ifPresentOrElse(
                        mayBeCustomerId.get(),
                        customer -> {
                            Optional<Contract> mayBeContract = customer.getContract(mayBeIdPartner.get(), mayBeIdContract.get());
                            if(mayBeContract.isPresent()) {
                                rc.response().putHeader(CONTENT_TYPE, APP_CHARSET)
                                        .setStatusCode(200)
                                        .end(ContractJsonCodec.toJson(mayBeContract.get()));
                            } else {
                                rc.response().setStatusCode(404).end();
                            }
                        },
                        () -> rc.response().setStatusCode(404).end()
                );
            } else {
                rc.response().setStatusCode(400).end();
            }
        } catch (NumberFormatException e2) {
            // Exception thrown by parseInt
            log.severe(e2.getMessage());
            rc.response().setStatusCode(400).end();
        }
    }

    /**
     * get a customer with an id
     * @param rc routing context
     */
    @Route(path = "/customers", methods = Route.HttpMethod.GET, produces = MediaType.APPLICATION_JSON, order = 1)
    public void getCustomers(RoutingContext rc) {

        Optional<String> mayBeExternalId = Optional.ofNullable(rc.request().getParam("external_id"));
        Optional<String> mayBeContractId = Optional.ofNullable(rc.request().getParam("contract_id"));

        directoryRepository.getDirectory().thenAccept(directory ->  {
            List<JsonObject> result = customerRepository.getCustomers(directory.searchCustomers(mayBeExternalId, mayBeContractId)).stream()
                    .map(CompletableFuture::join)
                    .map(customer -> new JsonObject(CustomerJsonCodec.toJson(customer.getCustomerProjection())))
                    .collect(Collectors.toList());

            rc.response().putHeader(CONTENT_TYPE, APP_CHARSET)
                    .setStatusCode(200)
                    .send((new JsonArray(result)).toString());
        });
    }

    /**
     * get a customer with an id
     * @param rc routing context
     */
    @Route(path = "/customers/:customerId", methods = Route.HttpMethod.GET, consumes = MediaType.APPLICATION_JSON, order = 4)
    public void getCustomer(RoutingContext rc) {
        Optional<String> mayBeId = Optional.ofNullable(rc.pathParam(CUSTOMER_ID_STRING));
        if(mayBeId.isPresent()) {
            customerRepository.ifPresentOrElse(
                    mayBeId.get(),
                    customer -> {
                        if(customer.getCustomerProjection().deleted) {
                            rc.response().setStatusCode(404).end();
                        } else {
                            rc.response().putHeader(CONTENT_TYPE, APP_CHARSET)
                                    .setStatusCode(200)
                                    .end(CustomerJsonCodec.toJson(customer.getCustomerProjection()));
                        }
                    },
                    () -> rc.response().setStatusCode(404).end()
            );
        } else {
            rc.response().setStatusCode(404).end();
        }
    }

    /**
     * delete a contract
     * @param rc  routing context
     */
    @Route(path = "/customers/:customerId/contract/:idContract", methods = Route.HttpMethod.DELETE, order = 9)
    public void deleteContract(RoutingContext rc) {
        try {
            Optional<String> mayBeCreatorId = Optional.of(rc.user().principal().getString(USER_ID_STRING));
            Optional<String> mayBeCustomerId = Optional.of(rc.pathParam(CUSTOMER_ID_STRING));
            Optional<Integer> mayBeIdPartner = Optional.of(0);
            Optional<String> mayBeIdContract = Optional.of(rc.pathParam(ID_CONTRACT_STRING));

            if(mayBeCustomerId.isPresent() && mayBeIdContract.isPresent() && mayBeCreatorId.isPresent()) {
                customerRepository.ifPresentOrElse(
                        mayBeCustomerId.get(),
                        customer -> {
                            ResourceName creator = new ResourceName(ResourceName.DOMAIN_PROFILE, CUSTOMERS_PATH, mayBeCreatorId.get());
                            Optional<Contract> mayBeContract = customer.getContract(mayBeIdPartner.get(), mayBeIdContract.get());
                            if(mayBeContract.isPresent()) {
                                customer.deleteContract(mayBeIdPartner.get(), mayBeIdContract.get(), creator, eventPublisher);
                                rc.response().setStatusCode(204).end();
                            } else {
                                rc.response().setStatusCode(404).end();
                            }
                        },
                        () -> rc.response().setStatusCode(404).end()
                );
            } else {
                rc.response().setStatusCode(400).end();
            }
        } catch (NumberFormatException e2) {
            // Exception thrown by parseInt
            log.severe(e2.getMessage());
            rc.response().setStatusCode(400).end();
        }
    }

    /**
     * delete a customer
     * @param rc routing context
     */
    @Route(path = "/customers/:customerId", methods = Route.HttpMethod.DELETE, order = 6)
    public void deleteCustomer(RoutingContext rc) {
        Optional<String> mayBeCreatorId = Optional.of(rc.user().principal().getString(USER_ID_STRING));
        Optional<String> mayBeId = Optional.of(rc.pathParam(CUSTOMER_ID_STRING));

        if(mayBeCreatorId.isPresent() && mayBeId.isPresent()) {
            customerRepository.ifPresentOrElse(
                    mayBeId.get(),
                    customer -> {
                        ResourceName creator = new ResourceName(ResourceName.DOMAIN_PROFILE, CUSTOMERS_PATH, mayBeCreatorId.get());
                        customer.deleteCustomer(creator, eventPublisher);
                        rc.response().setStatusCode(204).end();
                    },
                    () -> rc.response().setStatusCode(404).end()
            );
        } else {
            rc.response().setStatusCode(400).end();
        }
    }

    private void createContract(RoutingContext rc, Customer customer, String contractId, Integer idPartner, String creatorId) {
        JsonObject body = rc.getBodyAsJson();
        body.put(ID_PARTNER_STRING, idPartner);
        body.put(ID_CONTRACT_STRING, contractId);
        Optional<Contract> mayBeContract = ContractJsonCodec.fromJson(body.toString());

        if(mayBeContract.isPresent()) {
            ResourceName creator = new ResourceName(ResourceName.DOMAIN_PROFILE, CUSTOMERS_PATH, creatorId);
            Contract contract = mayBeContract.get();
            if(contract.getEndDate().isPresent()) {
                customer.createContract(idPartner, contract.getIdContract(), customer.getId().getId(), contract.getType(), contract.getMeasuresPoints(), contract.getBeginDate(), contract.getEndDate().get(), creator, eventPublisher);
            } else {
                customer.createContract(idPartner, contract.getIdContract(), customer.getId().getId(), contract.getType(), contract.getMeasuresPoints(), contract.getBeginDate(), creator, eventPublisher);
            }
            rc.response()
                    .putHeader("Location", rc.normalizedPath() + "/" + mayBeContract.get().getIdContract())
                    .putHeader(CONTENT_TYPE, APP_CHARSET)
                    .setStatusCode(201)
                    .end(ContractJsonCodec.toJson(customer.getContract(idPartner, mayBeContract.get().getIdContract()).get()));
        } else {
            rc.response().setStatusCode(400).end();
        }
    }
    /**
     * create a contract
     * @param rc routing context
     */
    @Route(path = "/customers/:customerId/contract", methods = Route.HttpMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON , order = 7)
    public void postContract(RoutingContext rc) {
        try {
            Optional<String> mayBeCreatorId = Optional.ofNullable(rc.user().principal().getString(USER_ID_STRING));
            Optional<String> mayBeId = Optional.ofNullable(rc.pathParam(CUSTOMER_ID_STRING));
            Optional<Integer> mayBeIdPartner = Optional.ofNullable(Integer.parseInt(rc.request().getHeader(ID_PARTNER_STRING)));

            if(mayBeCreatorId.isPresent() && mayBeId.isPresent() && mayBeIdPartner.isPresent()) {
                customerRepository.ifPresentOrElse(
                        mayBeId.get(),
                        customer -> {
                            String contractId = UUID.randomUUID().toString();
                            this.createContract(rc, customer, contractId, mayBeIdPartner.get(), mayBeCreatorId.get());
                        },
                        () -> rc.response().setStatusCode(404).end()
                );
            } else {
                rc.response().setStatusCode(400).end();
            }
        }
        catch (NumberFormatException e) {
            // Exception thrown by parseInt
            log.info(e.getMessage());
            rc.response().setStatusCode(400).end();
        }
    }

    /**
     * create or update an contract
     * @param rc routing context
     */
    @Route(path = "/customers/:customerId/contract/:idContract", methods = Route.HttpMethod.PUT, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON, order = 8)
    public void putContract(RoutingContext rc) {
        try {
            Optional<String> mayBeCreatorId = Optional.ofNullable(rc.user().principal().getString(USER_ID_STRING));
            Optional<String> mayBeId = Optional.ofNullable(rc.pathParam(CUSTOMER_ID_STRING));
            Optional<Integer> mayBeIdPartner = Optional.ofNullable(Integer.parseInt(rc.request().getHeader(ID_PARTNER_STRING)));
            Optional<String> mayBeContractId = Optional.ofNullable(rc.pathParam(ID_CONTRACT_STRING));

            if(mayBeCreatorId.isPresent() && mayBeId.isPresent() && mayBeIdPartner.isPresent() && mayBeContractId.isPresent()) {
                customerRepository.ifPresentOrElse(
                        mayBeId.get(),
                        customer -> {
                            Optional<Contract> mayBeContract = customer.getContract(mayBeIdPartner.get(), mayBeContractId.get());

                            if(mayBeContract.isPresent()) {
                                //update contract : TO DO
                                rc.response()
                                        .putHeader(CONTENT_TYPE, APP_CHARSET)
                                        .setStatusCode(200)
                                        .end(ContractJsonCodec.toJson(mayBeContract.get()));
                            } else {
                                this.createContract(rc, customer, mayBeContractId.get(), mayBeIdPartner.get(), mayBeCreatorId.get());
                            }
                        },
                        () -> rc.response().setStatusCode(404).end()
                );
            } else {
                rc.response().setStatusCode(400).end();
            }
        }
        catch (NumberFormatException e) {
            // Exception thrown by parseInt
            log.info(e.getMessage());
            rc.response().setStatusCode(400).end();
        }
    }

    /**
     * create a register
     * @param rc routing context
     */
    @Route(path = "/customers/:customerId/contract/:idContract/register/:idRegister", methods = Route.HttpMethod.PUT, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON, order = 10)
    public void PutRegister(RoutingContext rc) {
        try {
            String creatorId = rc.user().principal().getString(USER_ID_STRING);
            String customerId = rc.pathParam(CUSTOMER_ID_STRING);
            Integer idPartner = Integer.parseInt(rc.request().getHeader(ID_PARTNER_STRING));
            String idContract = rc.pathParam(ID_CONTRACT_STRING);
            String idRegister= rc.pathParam(ID_REGISTER_STRING);
            JsonObject body = rc.getBodyAsJson();
            ResourceName creator = new ResourceName(ResourceName.DOMAIN_PROFILE, CUSTOMERS_PATH, creatorId);
            customerRepository.getById(customerId)
                    .thenAccept((customer) -> {
                        customer.changeRegisterUri(idPartner, idRegister,body.getString("uri"), creator, eventPublisher);
                        Register register = null;
                        for (Contract contract : customer.getContractList()) {
                            if(contract.getIdContract().equals(idContract)){
                                for (MeasurePoint measuresPoints : contract.getMeasuresPoints()) {
                                    for (Register r : measuresPoints.getRegister()) {
                                        if (r.getIdPartner().equals(idPartner) && r.getIdRegister().equals(idRegister)) {
                                            register = r;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        rc.response().putHeader(CONTENT_TYPE, APP_CHARSET)
                                .setStatusCode(200)
                                .end(MeasurePointJsonCodec.RegisterToJson(register));
                    });

        } catch (NoSuchElementException e) {
            rc.response().setStatusCode(404).end();
        } catch (IllegalArgumentException e) {
            rc.response().setStatusCode(400).end();
        }
    }

}
