package com.domosnap.customer.infra.handlers.metric;

import java.util.logging.Logger;

import com.domosnap.tools.core.services.cqrs.domain.Event;
import com.domosnap.tools.core.services.cqrs.infra.handler.SingleEventPublisher;

public class ExampleHandler extends SingleEventPublisher {

		protected <T extends Event<?>> void register(Class<T> eventClass) {
		register(eventClass, this::apply);
	}

	public void apply(Event<?> event) {
		Logger log = Logger.getLogger(ExampleHandler.class.getName());
		try {
		
		} catch (Exception e) {
			log.severe("Metric error: impossible to send event. Details: ".concat(e.getMessage()));
		}
	}

	public ExampleHandler(String projectToken) {
		//register(UserChangeEvent.class);
	}

}
