package com.domosnap.customer.infra.handlers;

import java.util.function.Consumer;

import com.domosnap.customer.domain.user.events.CustomerCreateContractEvent;
import com.domosnap.customer.domain.user.events.CustomerCreatedEvent;
import com.domosnap.customer.domain.user.events.CustomerDeleteContractEvent;
import com.domosnap.customer.domain.user.events.CustomerDeleteEvent;
import com.domosnap.customer.infra.port.driven.repositories.DirectoryRepository;
import com.domosnap.tools.core.services.cqrs.domain.Event;
import com.domosnap.tools.core.services.cqrs.infra.handler.SingleEventPublisher;

public class UpdateDirectoryHandler extends SingleEventPublisher {

	private DirectoryRepository directoryRepository;
    private Consumer<Event<?>> dispatchEvent;

    /**
     * this will create a update directoryhandler 
     * @param directoryRepository directory repository
     * @param dispatchEvent consumer
     */
	public UpdateDirectoryHandler(DirectoryRepository directoryRepository, Consumer<Event<?>> dispatchEvent) {
		this.directoryRepository = directoryRepository;
		this.dispatchEvent = dispatchEvent;
        register(CustomerCreatedEvent.class, this::apply);
        register(CustomerDeleteEvent.class, this::apply);
        register(CustomerCreateContractEvent.class, this::apply);
        register(CustomerDeleteContractEvent.class, this::apply);
	}	
	
	public void apply(CustomerCreatedEvent event) {
		this.directoryRepository.getDirectory().thenAccept(
            directory -> {
                directory.addCustomer(event.getAggregateId(), event.getExternalId(), event.getCreator(), dispatchEvent);
            });
	}

    public void apply(CustomerDeleteEvent event) {
    	this.directoryRepository.getDirectory().thenAccept(
            directory -> {
                directory.removeCustomer(event.getAggregateId(), event.getCreator(), dispatchEvent);
            });
	}

    public void apply(CustomerCreateContractEvent event) {
    	this.directoryRepository.getDirectory().thenAccept(
            directory -> {
                directory.addContract(event.getAggregateId(), event.getIdContract(), event.getCreator(), dispatchEvent);
            });
	}

    public void apply(CustomerDeleteContractEvent event) {
    	this.directoryRepository.getDirectory().thenAccept(
            directory -> {
                directory.removeContract(event.getAggregateId(), event.getIdContract(), event.getCreator(), dispatchEvent);
            });
	}
}
