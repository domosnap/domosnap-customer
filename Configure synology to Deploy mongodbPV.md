# Domosnap-customer-management

## Setup the Synology NFS share



*    Control Panel/Shared Folder/Create
*    Fill out the info; name=”mongodb-data”, description=”MongoDB NFS share.”
*    uncheck “Enable Recycling Bin”
*    Click “next”; On the encryption screen click “Next”, on the advanced settings screen click “Next”; Confirm your settings and click “Apply”
*    The “Edit Shared Folder” window should pop up; select the “NFS Permissions” tab and click “Create”
*    In “Hostname or IP” add ‘*’ or if your kube cluster is on a specific network, add that info. Wildcard matching is insecure and shouldn’t be used for more than testing.
*    Make sure the following are all checked: Enable asynchronous; Allow connections from non-privileged ports; and allow users to access mounted subfolders. Click OK, and then click OK again.


Finalement je n'utilise pas les PV mais je fais pointer directement le pod sur le nfs.

## Lancer MongoDB en standalone sur x64_86:

sudo docker run -d mongo:3

## Lancer MongoDB en standalone sur raspberry:

### Starting MongoDB with Auth enabled. 

**First Read [How to setup auth in MongoDB 3.0 Properly](https://medium.com/@matteocontrini/how-to-setup-auth-in-mongodb-3-0-properly-86b60aeef7e8)**

```bash
$ docker run -d \
--name rpi3-mongodb3 \
--restart unless-stopped \
-v /data/db:/data/db \
-v /data/configdb:/data/configdb \
-p 27017:27017 \
-p 28017:28017 \
andresvidal/rpi3-mongodb3:latest \
mongod --auth
```

Add the initial Admin User. Replace `[username]` and `[password]`

**From CLI**

```
# creating a root user

$ docker exec -it rpi3-mongodb3 mongo admin --eval "db.createUser({user: '[username]', pwd: '[password]', roles:[{role:'root',db:'admin'}]});"
```

**From Container's terminal**

```
$ docker exec -it rpi3-mongodb3 mongo admin

connecting to: admin

> db.createUser({ user: "[username]", pwd: "[password]", roles: [ { role: "userAdminAnyDatabase", db: "admin" } ] })
```

Check the user has been created successfully

```
> db.auth("admin", "adminpassword")
```

Restart Container to ensure policies have applied.

**\#TODO:** update docker entry script to allow SIGINT to gracefully stop MongoDB on stop/restart container.

```
$ docker kill --signal=SIGINT rpi3-mongodb3
$ docker start rpi3-mongodb3
```

Creating a Database and add a User with permissions

```
$ docker exec -it rpi3-mongodb3 mongo admin
> db.auth("admin", "adminpassword")

> use yourdatabase
> db.createUser({ user: "youruser", pwd: "yourpassword", roles: [{ role: "dbOwner", db: "yourdatabase" }] })

> db.auth("youruser", "yourpassword")
> show collections
```

Read more on how to [Grant User Roles](https://docs.mongodb.com/manual/reference/method/db.grantRolesToUser/#db.grantRolesToUser)

*FYI:* The connection string to MongoDB for your application will look like this:

```
mongodb://youruser:yourpassword@localhost/yourdatabase
```

### Starting MongoDB with REST enabled.

```
docker run -d \
--name rpi3-mongodb3 \
--restart unless-stopped \
-v /data/db:/data/db \
-v /data/configdb:/data/configdb \
-p 27017:27017 \
-p 28017:28017 \
andresvidal/rpi3-mongodb3:latest \
mongod --rest
```