# Glossary 

### Event Sourcing

- Defines an approach to handling opearations on data that's driven by a sequence of events, each of which is recorded in an append-only store. 
- The events are persisted in an event store that acts as the system of record about the current state of the data.

### Command Query Responsability Segregation

CQRS separates and writes into different models, using commands to update data and queries to read data.

- Command should be task based, rather than data centric. (Book hotel room, not set ReservationStatus to Reserved)
- Commands may be placed on a queue for asynchronous processing, rather than being processed synchronously.
- Queries never modify the database. A query returns a DTO that does not encapsulate any domain knowledge.
- Benefits : independent scaling, optimitized data schemas, security, separation of concerns and simpler queries.

### Domain-Driven design

DDD advocates modelling based on the reality of business as relevant to your use cases. DDD describes independent problem (domain) areas as Bounded Contexts and emphasize a common language to talk about these problems.

### Aggregation

- Is a function where the values of multiple events/rows are grouped together to form a single summary value. 
- In this context, aggregations are applied to different events to recreate the current state of an entity.

### Projection



### Future vs Promise

Both refer to construct used for synchronizing program execution in some concurrent programming languages. Futures and promises originated in functional programming and related paradigms to decouple a value (a future) from how it was computed (a promise).

- Future : is a placeholder object for a value that may not yet exist. Read-only.
- Promise : is also placeholder object for a result that may not yet exist, but can also be thought of as a writable, single-assignment container, which completes a future.

### Java Future & CompletableFuture

- Java Future : a basic future which contains only these functions, cancel, get, isCancelled, isDone. 
- Java CompletableFuture : is a promise approach of Java and as its name indicates, it is a future that can be completable, thus contains much more interesting functions than the Future java.

### Vertx Future 

Future and promise in vertx get along with as a placeholder object for asynchronous results. Future takes a promise as argument in order to write its content.

- Vertx Future : Represents the read-only of a placeholder object of an asynchronous result, has more interesting functions than Java Future for when the result is completed, failed or done. 
- Vertx Promise : Represents the writable of a placeholder object of an asynchronous result. 

### Sources

- https://en.wikipedia.org/wiki/Futures_and_promises
- https://docs.scala-lang.org/overviews/core/futures.html
- https://docs.oracle.com/en/java/javase/13/docs/api/java.base/java/util/concurrent/Future.html
- https://vertx.io/docs/apidocs/io/vertx/core/Future.html
- https://docs.microsoft.com/en-us/azure/architecture/patterns/cqrs
- https://docs.microsoft.com/en-us/azure/architecture/patterns/event-sourcing
- https://docs.microsoft.com/en-us/dotnet/architecture/microservices/microservice-ddd-cqrs-patterns/ddd-oriented-microservice

