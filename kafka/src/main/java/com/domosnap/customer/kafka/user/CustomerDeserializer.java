package com.domosnap.customer.kafka.user;

import org.apache.kafka.common.serialization.Deserializer;

import com.domosnap.customer.domain.user.CustomerProjection;
import com.domosnap.customer.infra.port.driving.port.model.CustomerJsonCodec;


public class CustomerDeserializer implements Deserializer<CustomerProjection> {

    @Override
    public CustomerProjection deserialize(String topic, byte[] data) {
        String customerString = new String(data);
        System.out.println(customerString);
        return  CustomerJsonCodec.fromJson(customerString);
    }
    
}
