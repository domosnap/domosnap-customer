package com.domosnap.customer.kafka.user;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.UUID;
import java.util.logging.Logger;


import com.domosnap.customer.kafka.TimeFilteringTransformer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.*;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.common.utils.Bytes;

import com.domosnap.customer.kafka.consumption.CustomerConsumption;
import com.domosnap.customer.kafka.consumption.CustomerConsumptionAccumulator;

public class CustomersConsumptionKTable {
    
    private static final String BOOTSTRAP = "PLAINTEXT://localhost:9092";
    private final KafkaStreams streams;
    private final static String TOPIC_CONSUMPTION = "consumption";
    private final static String propFileName = "config.properties";
    private static final Logger log = Logger.getLogger(CustomersConsumptionKTable.class.getName());

    public CustomersConsumptionKTable() {
        final StreamsBuilder builder = new StreamsBuilder();
        KStream<String, String> sourceConsumptionStr = builder.stream(TOPIC_CONSUMPTION);

        KStream<String, CustomerConsumption> sourceConsumption = sourceConsumptionStr.mapValues(CustomerConsumption::fromJson);

        KTable<String, CustomerConsumptionAccumulator> consumptionAccumulatorKTable = accumulateConsumption(sourceConsumption, "materialized-consumption");

        KTable<String, CustomerConsumptionAccumulator> consumptionAccumulatorKTableFiltered = accumulateConsumptionBetweenTimestamps(sourceConsumption, "materialized-consumption-filtered", 1626360505609L, 1626360525615L);

        consumptionAccumulatorKTable.toStream().foreach((key, consumptionAcc) -> {
            System.out.println("Not filtered : " + key + " : " + consumptionAcc.toJson());
        });

        consumptionAccumulatorKTableFiltered.toStream().foreach((key, consumptionAcc) -> {
            System.out.println("Filtered : " + key + " : " + consumptionAcc.toJson());
        });

        this.streams = new KafkaStreams(builder.build(), this.getProps());
    }

    private static KTable<String, CustomerConsumptionAccumulator> accumulateConsumptionBetweenTimestamps(KStream<String, CustomerConsumption> sourceConsumption, String materializedName, long start, long end) {
        KStream<String, CustomerConsumption> sourceConsumptionFiltered = sourceConsumption
                .flatTransformValues(new TimeFilteringTransformer<>(start, end));
        return accumulateConsumption(sourceConsumptionFiltered, materializedName);
    }

    private static KTable<String, CustomerConsumptionAccumulator> accumulateConsumption(KStream<String, CustomerConsumption> sourceConsumption, String materializedName) {
        return sourceConsumption
                .groupByKey()
                .aggregate(
                        () -> CustomerConsumptionAccumulator.builder().build(),
                        (key, consumption, consumptionAccumulator) -> CustomerConsumptionAccumulator.builder(consumptionAccumulator).addConsumption(consumption).build(),
                        Materialized.<String, CustomerConsumptionAccumulator, KeyValueStore<Bytes,byte[]>>as(materializedName)
                                .withKeySerde(Serdes.String())
                                .withValueSerde(
                                        Serdes.serdeFrom(
                                                (topic, consumption) -> consumption.toJson().getBytes(),
                                                (topic, bytes) -> CustomerConsumptionAccumulator.fromJson(new String(bytes))
                                        )
                                )
                );
    }


    private Properties getProps() {
        Properties props = new Properties();
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
        
        try {
        	if(inputStream == null) {
        		throw new IOException();
        	}
        	props.load(inputStream);
        } catch (IOException e) {
        	log.finest("File config.properties not found or unreadable. Use default configuration.");
        	props.put(StreamsConfig.APPLICATION_ID_CONFIG, UUID.randomUUID().toString()); //make the difference, doesnt store a state
            props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, CustomersConsumptionKTable.BOOTSTRAP);    // assuming that the Kafka broker this application is talking to runs on local machine with port 9092
            props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
            props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
            props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
		}
        return props;
    }

    public void start() {
        this.streams.start();
    }

    public void close() {
        this.streams.close();
    }

}

