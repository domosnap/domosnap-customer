package com.domosnap.customer.kafka.user;

import java.io.InputStream;
import java.util.Properties;
import java.util.UUID;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.state.KeyValueStore;

import com.domosnap.customer.domain.user.CustomerProjection;
import com.domosnap.tools.core.services.cqrs.infra.eventStore.EventCodec;
    
public class CustomersKTable {
    
    private static String BOOTSTRAP = "PLAINTEXT://localhost:9092";
    private KafkaStreams streams;
    private final static String TOPIC = "customers";

    public CustomersKTable() {
        final StreamsBuilder builder = new StreamsBuilder();

        try {
            KStream<String, String> source = builder.stream(TOPIC);

            KTable<String, CustomerProjection> customers = source
            .mapValues((key, value) -> {
                System.out.println(value);
                return EventCodec.fromJson(value);
            })
            .groupByKey()
            .aggregate(() -> new CustomerProjection(), (key, event, customer) -> {
                    System.out.println(key);
                    System.out.println(event);
                    customer.apply(event);
                    return customer;
                }, 
                Materialized.<String, CustomerProjection, KeyValueStore<Bytes,byte[]>>as("materialized-customer-2")
                    .withKeySerde(Serdes.String())
                    .withValueSerde(Serdes.serdeFrom(new CustomerSerializer(), new CustomerDeserializer()))
                );

            customers.toStream().foreach((key, value) -> {
                System.out.println(key);
                System.out.println(value);
            });
                
            this.streams = new KafkaStreams(builder.build(), this.getProps());
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        // this.streams.setStateListener(new KafkaStreams.StateListener(){
        //     @Override
        //     public void onChange(State newState, State oldState) {
        //         if(KafkaStreams.State.RUNNING.equals(newState)) {
        //             try {
        //                 ReadOnlyKeyValueStore<String, Customer> keyValueStore = CustomersKTable.this.streams
        //                     .store(StoreQueryParameters.fromNameAndType("materialized-customer", QueryableStoreTypes.keyValueStore()));
            
        //                 Customer customer = keyValueStore.get("dd9f4fb6-9668-47df-8468-c6aa478a69b0");
        //                 System.out.println(customer);
        //             }
        //             catch(Exception e) {
        //                 e.printStackTrace();
        //             }
        //         }
        //     }
        // });

    }

    private Properties getProps() {
        Properties props = new Properties();
        String propFileName = "config.properties";
        try {
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
            if(inputStream == null) {
                throw new java.io.IOException("File " + propFileName + " not found. Default values taken in consideration.");
            }
            props.load(inputStream);
        }
        catch(java.io.IOException exception) {
            props.put(StreamsConfig.APPLICATION_ID_CONFIG, UUID.randomUUID().toString()); //make the difference, doesnt store a state
            props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, CustomersKTable.BOOTSTRAP);    // assuming that the Kafka broker this application is talking to runs on local machine with port 9092
            props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
            props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
            props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        }
        
        return props;
    }

    public void start() {
        try {
            this.streams.start();
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void close() {
        this.streams.close();
    }

}