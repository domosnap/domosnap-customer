package com.domosnap.customer.kafka.consumption;

import com.domosnap.customer.domain.user.CustomerProjection;
import com.domosnap.customer.infra.port.driving.port.model.CustomerJsonCodec;
import io.vertx.core.json.JsonObject;

import java.util.Optional;

public class CustomerConsumptionAccumulator {
    private CustomerProjection customer;
    private Double power;

    private CustomerConsumptionAccumulator(CustomerProjection customer, Double power) {
        this.customer = customer;
        this.power = power; // en Watts
    }

    public String toJson() {
        return (new StringBuilder(""))
            .append("{\"customer\":")
            .append(CustomerJsonCodec.toJson(this.customer))
            .append(",\"power\":")
            .append(this.power).append("}")
            .toString();
    }

    public static CustomerConsumptionAccumulator fromJson(String json) {
        JsonObject jo = new JsonObject(json);
        JsonObject customer = jo.getJsonObject("customer");
        return new CustomerConsumptionAccumulator(CustomerJsonCodec.fromJson(customer.toString()), jo.getDouble("power"));
    }

    public static Builder builder(CustomerConsumptionAccumulator customerConsumptionAcc) {
        return new Builder(customerConsumptionAcc);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private Optional<CustomerProjection> customer;
        private Optional<Double> power;

        private Builder(CustomerProjection customer, double power) {
            this.customer = Optional.of(customer);
            this.power = Optional.of(power);
        }

        private Builder() {
            this.customer = Optional.of(new CustomerProjection());
            this.power = Optional.of(0.0);
        }

        private Builder(CustomerConsumption customerConsumption) {
            this(customerConsumption.getCustomer(), customerConsumption.getWatts());
        }

        private Builder(CustomerConsumptionAccumulator customerConsumptionAcc) {
            this(customerConsumptionAcc.customer, customerConsumptionAcc.power);
        }

        public Builder addConsumption(CustomerConsumption customerConsumption) {
            this.customer = Optional.of(customerConsumption.getCustomer());
            this.power = Optional.of(this.power.orElse(0.0) + customerConsumption.getWatts());
            return this;
        }

        public CustomerConsumptionAccumulator build() {
            return new CustomerConsumptionAccumulator(this.customer.orElse(null), this.power.orElse(0.0));
        }
    }

}