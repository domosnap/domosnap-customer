package com.domosnap.customer.kafka.consumption;

import com.domosnap.customer.domain.user.CustomerProjection;
import com.domosnap.customer.infra.port.driving.port.model.CustomerJsonCodec;
import com.domosnap.engine.controller.what.impl.IntegerState;
import com.domosnap.engine.controller.what.impl.WattState;

import io.vertx.core.json.JsonObject;

public class CustomerConsumption {
    private CustomerProjection customer;
    private Consumption consumption;

    public CustomerConsumption(CustomerProjection customer, Consumption consumption) {
        this.customer = customer;
        this.consumption = consumption;
    }

    public CustomerConsumption(Consumption consumption, CustomerProjection customer) {
        this(customer, consumption);
    }

    public CustomerProjection getCustomer() {
        return this.customer;
    }

    public Consumption getConsumption() {
        return this.consumption;
    }

    public double getWatts() {
        return this.consumption
            .getEventCommand()
            .getCommand()
            .getWhatList() // list of whats
            .stream()
            .map(w -> (WattState)w.getValue()) // Get State of What
            .mapToInt(IntegerState::getValue) // Get value of WattState
            .sum();
    }

    public String toJson() {
        return (new StringBuilder(""))
            .append("{\"customer\":")
            .append(CustomerJsonCodec.toJson(this.customer))
            .append(",\"consumption\":")
            .append(this.consumption.toJson()).append("}")
            .toString();
    }

    public static CustomerConsumption fromJson(String json) {
        JsonObject jo = new JsonObject(json);
        JsonObject customer = jo.getJsonObject("customer");
        JsonObject consumption = jo.getJsonObject("consumption");
        return new CustomerConsumption(CustomerJsonCodec.fromJson(customer.toString()), Consumption.fromJson(consumption.toString()));
    }
}
