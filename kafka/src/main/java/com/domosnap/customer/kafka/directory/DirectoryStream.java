package com.domosnap.customer.kafka.directory;

import java.util.Properties;
import java.util.UUID;
import java.util.function.BiConsumer;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;

import com.domosnap.customer.domain.directory.Directory;
import com.domosnap.tools.core.services.cqrs.domain.Event;
import com.domosnap.tools.core.services.cqrs.infra.eventStore.EventCodec;

public class DirectoryStream {
    
    private static String BOOTSTRAP = "PLAINTEXT://localhost:9092";
    private KafkaStreams streams;
    private Directory directory;

    public DirectoryStream(BiConsumer<Event<?>, Directory> consumer) {
        this.directory = new Directory();
        final StreamsBuilder builder = new StreamsBuilder();
        KStream<String, String> source = builder.stream(Directory.mainDirectory.toString());
        source.mapValues(value -> EventCodec.fromJson(value)).foreach((key, event) -> {
            System.out.println(key);
            directory.apply(event);
            consumer.accept(event, directory);
        });
        this.streams = new KafkaStreams(builder.build(), this.getProps());
    }

    private Properties getProps() {
        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, UUID.randomUUID().toString()); //make the difference, doesnt store a state
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, DirectoryStream.BOOTSTRAP);    // assuming that the Kafka broker this application is talking to runs on local machine with port 9092
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        return props;
    }

    public void start() {
        this.streams.start();
    }

    public void close() {
        this.streams.close();
    }

}