package com.domosnap.customer.kafka.user;

import org.apache.kafka.common.serialization.Serializer;

import com.domosnap.customer.domain.user.CustomerProjection;
import com.domosnap.customer.infra.port.driving.port.model.CustomerJsonCodec;

public class CustomerSerializer implements Serializer<CustomerProjection> {

    @Override
    public byte[] serialize(String topic, CustomerProjection customer) {
        return CustomerJsonCodec.toJson(customer).getBytes();
    }
    
}
