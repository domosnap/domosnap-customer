package com.domosnap.customer.kafka;

import org.apache.kafka.streams.kstream.ValueTransformer;
import org.apache.kafka.streams.kstream.ValueTransformerSupplier;
import org.apache.kafka.streams.processor.ProcessorContext;

import java.util.ArrayList;
import java.util.List;

public final class TimeFilteringTransformer<T> implements ValueTransformerSupplier<T, Iterable<T>> {
    private final long earliest;
    private final long latest;

    public TimeFilteringTransformer(final long earliest, final long latest) {
        this.earliest = earliest;
        this.latest = latest;
    }

    @Override
    public ValueTransformer<T, Iterable<T>> get() {
        return new ValueTransformer<>() {
            private ProcessorContext processorContext;

            @Override
            public void init(ProcessorContext context) {
                this.processorContext = context;
            }

            @Override
            public Iterable<T> transform(T value) {
                long ts = processorContext.timestamp();
                System.out.println("Timestamp : " + Long.toString(ts));
                if (ts >= earliest && ts <= latest) {
                    System.out.println("Getting one value");
                    return List.of(value);
                }
                return new ArrayList<>();
            }

            @Override
            public void close() {

            }
        };
    }
}
