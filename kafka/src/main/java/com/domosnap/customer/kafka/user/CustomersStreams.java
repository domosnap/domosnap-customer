package com.domosnap.customer.kafka.user;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.function.BiConsumer;
import java.util.logging.Logger;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;

import com.domosnap.customer.domain.user.CustomerProjection;
import com.domosnap.tools.core.services.cqrs.domain.Event;
import com.domosnap.tools.core.services.cqrs.infra.eventStore.EventCodec;

public class CustomersStreams {
    
    private static String BOOTSTRAP = "PLAINTEXT://localhost:9092";
    private KafkaStreams streams;
    private final static String TOPIC = "customers";
    private final static String propFileName = "config.properties";
    private static final Logger log = Logger.getLogger(CustomersStreams.class.getName());

    public CustomersStreams(BiConsumer<Event<?>, CustomerProjection> consumer) {
        Map<String, CustomerProjection> customers = new HashMap<>();
        final StreamsBuilder builder = new StreamsBuilder();
        KStream<String, String> source = builder.stream(TOPIC);
        source.mapValues(value -> EventCodec.fromJson(value)).foreach((key, event) -> {
        	CustomerProjection customer = customers.getOrDefault(key, new CustomerProjection());
            customer.apply(event);
            customers.put(key, customer);
            consumer.accept(event, customer);
        });
        this.streams = new KafkaStreams(builder.build(), this.getProps());
    }

    private Properties getProps() {
        Properties props = new Properties();
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
        
        try {
        	if(inputStream == null) {
        		throw new IOException();
        	}
        	props.load(inputStream);
        } catch (IOException e) {
        	log.finest("File config.properties not found or unreadable. Use default configuration.");
        	props.put(StreamsConfig.APPLICATION_ID_CONFIG, UUID.randomUUID().toString()); //make the difference, doesnt store a state
            props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, CustomersStreams.BOOTSTRAP);    // assuming that the Kafka broker this application is talking to runs on local machine with port 9092
            props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
            props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
            props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
		}
        return props;
    }

    public void start() {
        this.streams.start();
    }

    public void close() {
        this.streams.close();
    }

}