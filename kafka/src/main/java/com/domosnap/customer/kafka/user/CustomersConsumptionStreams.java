package com.domosnap.customer.kafka.user;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.UUID;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.List;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.kstream.Joined;
import org.apache.kafka.streams.kstream.Produced;

import com.domosnap.tools.core.services.cqrs.domain.Event;
import com.domosnap.tools.core.services.cqrs.infra.eventStore.EventCodec;
import com.domosnap.customer.domain.user.CustomerProjection;
import com.domosnap.customer.domain.user.events.CustomerCreateContractEvent;
import com.domosnap.customer.kafka.consumption.Consumption;
import com.domosnap.customer.kafka.consumption.CustomerConsumption;

import com.domosnap.engine.event.EventJsonCodec;


public class CustomersConsumptionStreams {
    
    private static String BOOTSTRAP = "PLAINTEXT://localhost:9092";
    private final KafkaStreams streams;
    private final static String TOPIC_CUSTOMER = "customers";
    private final static String TOPIC_DEVICES = "domosnap-event-topic";
    private final static String propFileName = "config.properties";
    private static final Logger log = Logger.getLogger(CustomersStreams.class.getName());

    public CustomersConsumptionStreams() {

            final StreamsBuilder builder = new StreamsBuilder();
            KStream<String, String> sourceCustomers = builder.stream(TOPIC_CUSTOMER);
            KStream<String, Event> sourceCustomerEvents = sourceCustomers.mapValues(EventCodec::fromJson);

            // id_customer -> customer
            KTable<String, CustomerProjection> customersKTable = aggregateCustomerKTable(sourceCustomerEvents);

            // id_device -> id_customer
            KTable<String, String> directoryDevices = aggregateDeviceKTable(sourceCustomerEvents);

            // id_device -> device consumption as string
            KStream<String, String> sourceDevices = builder.stream(TOPIC_DEVICES);
            
            KStream<String, com.domosnap.engine.event.Event> sourceEventCommands = sourceDevices.mapValues(EventJsonCodec::fromJson);

            // id_customer -> device consumption
            KStream<String, Consumption> consumptionByCustomerId = joinDeviceWithCustomerId(sourceEventCommands, directoryDevices);
        
            // id_customer -> customer with consumption
            KStream<String, CustomerConsumption> customersConsumption = joinConsumptionWithCustomer(consumptionByCustomerId, customersKTable);

            publishConsumptionOnTopic(customersConsumption, "consumption");

            this.streams = new KafkaStreams(builder.build(), this.getProps());
    }

    private Properties getProps() {
        Properties props = new Properties();
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
        
        try {
        	if(inputStream == null) {
        		throw new IOException();
        	}
        	props.load(inputStream);
        } catch (IOException e) {
        	log.finest("File config.properties not found or unreadable. Use default configuration.");
        	props.put(StreamsConfig.APPLICATION_ID_CONFIG, UUID.randomUUID().toString()); //make the difference, doesnt store a state
            props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, CustomersConsumptionStreams.BOOTSTRAP);    // assuming that the Kafka broker this application is talking to runs on local machine with port 9092
            props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
            props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
            props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
		}
        return props;
    }

    public void start() {
        this.streams.start();
    }

    public void close() {
        this.streams.close();
    }

    private static KTable<String, CustomerProjection> aggregateCustomerKTable(KStream<String, Event> sourceCustomerEvents) {
        // id_customer -> customer
        return sourceCustomerEvents
            .groupByKey()
            .aggregate(CustomerProjection::new, (key, event, customer) -> {
                customer.apply(event);
                return customer;
            }, 
            Materialized.<String, CustomerProjection, KeyValueStore<Bytes,byte[]>>as("materialized-customer")
                .withKeySerde(Serdes.String())
                .withValueSerde(Serdes.serdeFrom(new CustomerSerializer(), new CustomerDeserializer()))
            );
    }

    private static KTable<String, String> aggregateDeviceKTable(KStream<String, Event> sourceCustomerEvents) {
        // id_device -> id_customer
        return sourceCustomerEvents
            .filter((key, event) -> event instanceof CustomerCreateContractEvent)
            .flatMap((keyCustomer, value) -> {
                        CustomerCreateContractEvent event = (CustomerCreateContractEvent)value;
                        List<KeyValue<String, String>> registersCustomers = event
                            .getMeasuresPoints()
                            .stream()
                            .flatMap(m -> m.getRegister().stream())
                            .map(r -> new KeyValue<>(r.getUri(), event.getIdCustomer()))
                            .collect(Collectors.toList());

                        return registersCustomers;
                    
                })
            .groupByKey()
            .aggregate(() -> "", (idDevice, newIdCustomer, oldIdCustomer) -> {
                    return newIdCustomer;
                },
                Materialized.<String, String, KeyValueStore<Bytes, byte[]>>as("materilized-customer-by-device")
                    .withKeySerde(Serdes.String())
                    .withValueSerde(Serdes.String())
            );
    }

    private static KStream<String, Consumption> joinDeviceWithCustomerId(KStream<String, com.domosnap.engine.event.Event> sourceEventCommands, KTable<String, String> directoryDevicesCustomers) {
        // id_customer -> device consumption
        return sourceEventCommands
            .join(directoryDevicesCustomers, Consumption::new)
            .map((idDevice, consumption) -> new KeyValue<>(consumption.getCustomerId(), consumption));
    
    }

    private static KStream<String, CustomerConsumption> joinConsumptionWithCustomer(KStream<String, Consumption> sourceConsumption, KTable<String, CustomerProjection> customers) {
        // id_customer -> customer with consumption
        return sourceConsumption
            .join(
                customers,
                    CustomerConsumption::new,
                Joined.with(
                    Serdes.String(), 
                    Serdes.serdeFrom(
                            (topic, consumption) -> consumption.toJson().getBytes(),
                            (topic, bytes) -> Consumption.fromJson(new String(bytes))
                    ),
                    Serdes.serdeFrom(new CustomerSerializer(), new CustomerDeserializer())
                )
            );
    }

    private static void publishConsumptionOnTopic(KStream<String, CustomerConsumption> consumptionCustomerSource, String topic) {
        consumptionCustomerSource
            .mapValues(CustomerConsumption::toJson)
            .to(topic, Produced.with(Serdes.String(), Serdes.String()));
    } 

}