package com.domosnap.customer.kafka.consumption;

import com.domosnap.engine.event.Event;
import com.domosnap.engine.event.EventJsonCodec;

import io.vertx.core.json.JsonObject;

public class Consumption {

    private String customerId;
    private Event eventCommand;

    public Consumption(Event eventCommand, String customerId) {
        this.eventCommand = eventCommand;
        this.customerId = customerId;
    }

    public String getCustomerId() {
        return this.customerId;
    }

    public Event getEventCommand() {
        return this.eventCommand;
    }

    public String toJson() {
        return (new StringBuilder(""))
            .append("{\"command\":")
            .append(this.eventCommand.toString())
            .append(",\"customerId\":")
            .append("\"").append(this.customerId).append("\"}")
            .toString();
    }

    public static Consumption fromJson(String json) {
        JsonObject jo = new JsonObject(json);
        JsonObject command = jo.getJsonObject("command");
        return new Consumption(EventJsonCodec.fromJson(command.toString()), jo.getString("customerId"));
    }
}