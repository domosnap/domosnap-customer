package com.domosnap.customer.kafka;

import com.domosnap.customer.kafka.user.CustomersKTable;
import com.domosnap.customer.kafka.user.CustomersConsumptionStreams;
import com.domosnap.customer.kafka.user.CustomersConsumptionKTable;

public class Pipe {

    public static void main(String[] args) throws Exception {
        
        // CustomersStreams customersStreams = new CustomersStreams((event, customer) -> {
        //     if(customer.getId() != null) {
        //         try {
        //             File file = new File("file-uploads" + File.separator + customer.getId().getId() + ".json");
        //             PrintWriter writer = new PrintWriter(file, "UTF-8");
        //             writer.println(CustomerJsonCodec.toJson(customer));
        //             writer.close();
        //         }
        //         catch(IOException ex) {
        //             ex.printStackTrace();
        //         } 
        //     }
        // });
        
        // customersStreams.start();
    
        // Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
        //     @Override
        //     public void run() {
        //         customersStreams.close();
        //     }
        // }));
        
        // try {
        //     CustomersKTable customerKTable = new CustomersKTable();
        //     customerKTable.start();
        //     Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
        //         @Override
        //         public void run() {
        //             System.out.println("chao");
        //             customerKTable.close();
        //         }
        //     }));
        // }
        // catch(Exception e) {
        //     e.printStackTrace();
        // }

        try {
            // CustomersConsumptionStreams customerConsumption = new CustomersConsumptionStreams();
            //customerConsumption.start();

            CustomersConsumptionKTable customerConsumptionKTable = new CustomersConsumptionKTable();
            customerConsumptionKTable.start();

            Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
                @Override
                public void run() {
                    System.out.println("chao");
                    //customerConsumption.close();
                    customerConsumptionKTable.close();
                }
            }));
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        
    }
}