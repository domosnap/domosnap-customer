# Kafka 

## Install Kafka and launch
- Launch Zookeeper
```bin/zookeeper-server-start.sh config/zookeeper.properties```

- Launch Kafka
```bin/kafka-server-start.sh config/server.properties```

## Compile and launch app

- Compile
```mvn package```

- Launch
```mvn exec:java@Main```


