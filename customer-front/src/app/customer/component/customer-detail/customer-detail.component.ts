import { Component, Input, OnInit } from '@angular/core';
import { Customer } from '../../model/customer.model';

@Component({
  selector: 'ds-customer-detail',
  templateUrl: './customer-detail.component.html',
  styleUrls: ['./customer-detail.component.css']
  // inputs: ['customer: customerField']
})
export class CustomerDetailComponent implements OnInit {

@Input('customerField')
  customer : Customer = {'id':'id1', 'externalId':'Mon nom', contracts: []};
  
  constructor() { }

  ngOnInit(): void {
  }
  
  /*
  set customer(value: Customer) {
	console.log(value);
	this.cust = value;
  }
  
  get customer() : Customer {
	return this.cust;
  }*/

}
