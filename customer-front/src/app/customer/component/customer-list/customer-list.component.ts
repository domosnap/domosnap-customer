import { Component, EventEmitter, OnInit, Output } from '@angular/core';

import { Customer } from '../../model/customer.model';
import { CustomerService } from '../../service/customer.service';


@Component({
	selector: 'ds-customer-list',
	templateUrl: './customer-list.component.html',
	styleUrls: ['./customer-list.component.css']
})
export class CustomerListComponent implements OnInit {

	customers: Customer[];
	
	selectedCustomer: Customer;

	@Output() readonly customerSelected = new EventEmitter<Customer>();

	constructor(private customerService: CustomerService) { 
		 this.customers = [];
		 this.selectedCustomer = {id: '', externalId: '', contracts: []};
	}

	ngOnInit(): void {
	}

	refreshCustomers(): void {
//		this.customers = [{ name: 'London', id: '1' }, { name: 'Lyon', id: '2' }];
		
		this.customerService.list().forEach(e => this.customers = e);
	}
	
	selectCustomer(cust : Customer): void {
		this.customerSelected.emit(cust);
		this.selectedCustomer=cust;
	}

}
