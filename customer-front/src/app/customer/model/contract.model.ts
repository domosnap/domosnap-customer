

export interface Contract {
  idContract: string;
  type: string;
  beginDate: Date;
  endDate: Date;
}
