import { Contract } from '../model/contract.model';

export interface Customer {
  id: string;
  externalId: string;
  contracts: Array<Contract>;
}
