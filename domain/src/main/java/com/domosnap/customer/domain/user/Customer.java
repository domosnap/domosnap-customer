/* Customer
 * This the domain for the Customer
 * Raphaël Lopes
 * 06.06.2020
 */
package com.domosnap.customer.domain.user;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.logging.Logger;

import com.domosnap.customer.domain.user.events.CustomerChangeEvent;
import com.domosnap.customer.domain.user.events.CustomerChangeRegisterEvent;
import com.domosnap.customer.domain.user.events.CustomerCreateContractEvent;
import com.domosnap.customer.domain.user.events.CustomerCreatedEvent;
import com.domosnap.customer.domain.user.events.CustomerDeleteContractEvent;
import com.domosnap.customer.domain.user.events.CustomerDeleteEvent;
import com.domosnap.tools.core.services.cqrs.domain.Aggregate;
import com.domosnap.tools.core.services.cqrs.domain.Event;
import com.domosnap.tools.core.services.exception.AlreadyExistingElementException;
import com.domosnap.tools.core.services.resourcenames.ResourceName;
public class Customer implements Aggregate<CustomerId> {
	
	private Logger log = Logger.getLogger(Customer.class.getName());
    private CustomerProjection projection;
    
    public Customer(List<Event<?>> history) {
        projection = new CustomerProjection(history);
    }
     
    public static void create(CustomerId customerId, Integer idPartner, String externalId, ResourceName principal, Consumer<Event<?>> eventPublisher) {
    	eventPublisher.accept(new CustomerCreatedEvent(customerId, idPartner, externalId, principal));
    }
    /**
     * Create a Customer
     * @param customerId id of the customer
     * @param idPartner id of the partner
     * @param externalId external id
     * @param principal resource name
     * @param eventPublisher consumer
     */
    public void changeCustomerProperties(CustomerId customerId, Integer idPartner, String externalId, ResourceName principal, Consumer<Event<?>> eventPublisher) {
    	
    	Event<CustomerId> event = new CustomerChangeEvent(getId(),idPartner, externalId, principal);
    	projection.apply(event);
    	eventPublisher.accept(event);
    }
    /**
     * Change the register value
     * @param idPartner id of the partner
     * @param idRegister id of the register
     * @param uri new uri value
     * @param principal resource name
     * @param eventPublisher consumer
     */
    public void changeRegisterUri(Integer idPartner, String idRegister,String uri, ResourceName principal, Consumer<Event<?>> eventPublisher) {
    	Event<CustomerId> event = new CustomerChangeRegisterEvent(getId(),idPartner, idRegister,uri, principal);
    	projection.apply(event);
    	eventPublisher.accept(event);
    }
    /**
     * Create a contract for a customer
     * @param idPartner id of the partner
     * @param idContract id of the contract
     * @param idCustomer id of the customer
     * @param type type of the contract
     * @param measuresPoints measuresPoints for the contract
     * @param beginDate begin date of the contract
     * @param endDate end date of the contract
     * @param principal resource name
     * @param eventPublisher consumer
     */
    public void createContract(Integer idPartner,String idContract,String idCustomer,String type,List<MeasurePoint> measuresPoints, Date beginDate, Date endDate, ResourceName principal, Consumer<Event<?>> eventPublisher) {
		boolean exist = false;
		for (Contract Contract : projection.contractList) {
			if (Contract.getIdPartner().equals(idPartner) && Contract.getIdContract().equals(idContract)) {
				exist = true;
				break;
			}
		}
		if (!exist) {
			Event<CustomerId> event = new CustomerCreateContractEvent(getId(), idPartner,idContract,idCustomer,type,measuresPoints,beginDate,endDate,principal);
			projection.apply(event);
			eventPublisher.accept(event);
		} else {
			log.finest("Account [".concat(idPartner.toString()).concat("] already existing for user [")
					.concat(String.valueOf(getId())).concat("]."));
			throw new AlreadyExistingElementException("Account named [".concat(idPartner.toString()).concat("]."));
		}
	}

    public void createContract(Integer idPartner,String idContract,String idCustomer,String type,List<MeasurePoint> measuresPoints, Date beginDate, ResourceName principal, Consumer<Event<?>> eventPublisher) {
        boolean exist = false;
		for (Contract Contract : projection.contractList) {
			if (Contract.getIdPartner().equals(idPartner) && Contract.getIdContract().equals(idContract)) {
				exist = true;
				break;
			}
		}
		if (!exist) {
			Event<CustomerId> event = new CustomerCreateContractEvent(getId(), idPartner,idContract,idCustomer,type,measuresPoints,beginDate,principal);
			projection.apply(event);
			eventPublisher.accept(event);
		} else {
			log.finest("Account [".concat(idPartner.toString()).concat("] already existing for user [")
					.concat(String.valueOf(getId())).concat("]."));
			throw new AlreadyExistingElementException("Account named [".concat(idPartner.toString()).concat("]."));
		}
    }

    /**
     * Delete a customer
     * @param principal resource name
     * @param eventPublisher consumer
     */
    public void deleteCustomer(ResourceName principal, Consumer<Event<?>> eventPublisher) {
    	Event<CustomerId> event = new CustomerDeleteEvent(getId(), principal);
    	projection.apply(event);
    	eventPublisher.accept(event);
    }
    /**
     * delete a contract 
     * @param idPartner id of the partner
     * @param idContract id of the contract
     * @param principal resource name
     * @param eventPublisher consumer
     */
    public void deleteContract(Integer idPartner,String idContract, ResourceName principal, Consumer<Event<?>> eventPublisher) {
    	Event<CustomerId> event = new CustomerDeleteContractEvent(getId(),idPartner,idContract, principal);
    	projection.apply(event);
    	eventPublisher.accept(event);
    }
	
    // QUERY
    public CustomerId getId() {
        return projection.id;
    }
    
    public CustomerProjection getCustomerProjection() {
		return projection;
	}


	public List<Contract> getContractList() {
		return projection.contractList;
	}

	public Optional<Contract> getContract(Integer idPartner, String idContract) {
		for (Contract contract : projection.contractList) {
			if (idPartner.equals(contract.getIdPartner()) && idContract.equals(contract.getIdContract())) {
				return Optional.of(contract);
			}
		}
		return Optional.empty();
	}
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{\"id\":\"").append(getId()).append("\",")
          .append("\"idPartner\":\"").append(projection.idPartner).append("\",")
          .append("\"ExternalId\":\"").append(projection.externalId).append("\"}");
        return sb.toString();
    }

}
