package com.domosnap.customer.domain.directory.events;

import com.domosnap.customer.domain.directory.DirectoryId;
import com.domosnap.customer.domain.user.CustomerId;
import com.domosnap.tools.core.services.cqrs.domain.impl.EventBase;
import com.domosnap.tools.core.services.resourcenames.ResourceName;


public class DirectoryCustomerAddedEvent extends EventBase<DirectoryId> {
    private CustomerId customerId;
    private String externalId;

	public DirectoryCustomerAddedEvent(DirectoryId directoryId, CustomerId customerId, String externalId, ResourceName creator) {
		super(directoryId, creator);
        this.customerId = customerId;
        this.externalId = externalId;
	}

    public CustomerId getCustomerId() {
        return this.customerId;
    }

    public String getExternalId() {
        return this.externalId;
    }
}
