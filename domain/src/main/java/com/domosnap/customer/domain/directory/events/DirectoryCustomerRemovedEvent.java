package com.domosnap.customer.domain.directory.events;

import com.domosnap.customer.domain.directory.DirectoryId;
import com.domosnap.customer.domain.user.CustomerId;
import com.domosnap.tools.core.services.cqrs.domain.impl.EventBase;
import com.domosnap.tools.core.services.resourcenames.ResourceName;

public class DirectoryCustomerRemovedEvent extends EventBase<DirectoryId> {
    private CustomerId customerId;

    public DirectoryCustomerRemovedEvent(DirectoryId directoryId, CustomerId customerId, ResourceName creator) {
        super(directoryId, creator);
        this.customerId = customerId;
    }

    public CustomerId getCustomerId() {
        return this.customerId;
    }
}