package com.domosnap.customer.domain.directory;

import com.domosnap.tools.core.services.cqrs.domain.AggregateId;

public class DirectoryId implements AggregateId {
    private String value;
    
    public DirectoryId(String value) {
        this.value = value;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DirectoryId directoryId = (DirectoryId) o;

        return !(value != null ? !value.equals(directoryId.value) : directoryId.value != null);

    }

    @Override
    public int hashCode() {
        return value != null ? value.hashCode() : 0;
    }

    @Override
    public String toString() {
        return value;
    }

	@Override
	public String getId() {
		return value;
	}

}
