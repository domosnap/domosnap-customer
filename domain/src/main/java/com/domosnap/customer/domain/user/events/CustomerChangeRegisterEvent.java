/* CustomerChangeRegisterEvent
 * Event for when a register change
 * Raphaël Lopes
 * 06.06.2020
 */
package com.domosnap.customer.domain.user.events;

import com.domosnap.customer.domain.user.CustomerId;
import com.domosnap.tools.core.services.cqrs.domain.impl.EventBase;
import com.domosnap.tools.core.services.resourcenames.ResourceName;

public class CustomerChangeRegisterEvent extends EventBase<CustomerId> {

	private final Integer idPartner;
	private final String idRegistered;
	private final String uri;
	
	
	public CustomerChangeRegisterEvent(CustomerId customerId, Integer idPartner, String idRegistered, String uri, ResourceName creator) {
		super(customerId, creator);
		this.idPartner = idPartner;
		this.idRegistered = idRegistered;
		this.uri = uri;
	}

	public Integer getIdPartner() {
		return idPartner;
	}

	public String getIdRegistered() {
		return idRegistered;
	}

	public String getUri() {
		return uri;
	}
}
