package com.domosnap.customer.domain.user;

import java.util.ArrayList;
import java.util.List;

import com.domosnap.customer.domain.user.events.CustomerChangeEvent;
import com.domosnap.customer.domain.user.events.CustomerChangeRegisterEvent;
import com.domosnap.customer.domain.user.events.CustomerCreateContractEvent;
import com.domosnap.customer.domain.user.events.CustomerCreatedEvent;
import com.domosnap.customer.domain.user.events.CustomerDeleteContractEvent;
import com.domosnap.customer.domain.user.events.CustomerDeleteEvent;
import com.domosnap.tools.core.services.cqrs.domain.Event;
import com.domosnap.tools.core.services.cqrs.domain.impl.ProjectionBase;

public class CustomerProjection extends ProjectionBase {
	public List<Contract> contractList = new ArrayList<>();
	public CustomerId id;
	public Integer idPartner;
	public String externalId;
	public Boolean deleted = false;
	public List<Event<?>> history = new ArrayList<>();

	public CustomerProjection() {
    	register(CustomerCreatedEvent.class, this::apply);
        register(CustomerChangeEvent.class, this::apply);
        register(CustomerDeleteEvent.class, this::apply);
        register(CustomerCreateContractEvent.class, this::apply);
        register(CustomerDeleteContractEvent.class, this::apply);
        register(CustomerChangeRegisterEvent.class, this::apply);
    }
	
    public CustomerProjection(List<Event<?>> history) {
    	this();
    	history.forEach(this::apply);
    }
    
      
    public CustomerProjection(CustomerId id, Integer idPartner, String externalId,
    		List<Contract> contractList, Boolean deleted) {
		this();
		this.contractList = contractList;
		this.id = id;
		this.idPartner = idPartner;
		this.externalId = externalId;
		this.deleted = deleted;
	}

	public void apply(CustomerDeleteContractEvent event) {
    	for (Contract contract : contractList) {
    		if (event.getIdPartner().equals(contract.getIdPartner()) && event.getIdContract().equals(contract.getIdContract())) {
    			contractList.remove(contract);
    			break;
    		}
		}
    	history.add(event);
    }

    public void apply(CustomerCreateContractEvent event) {
        if(event.getEndDate().isPresent()) {
		    contractList.add(new Contract(event.getIdPartner(),event.getIdContract(),event.getIdCustomer(),event.getType(),event.getMeasuresPoints(),event.getBeginDate(),event.getEndDate().get()));
        } else {
		    contractList.add(new Contract(event.getIdPartner(),event.getIdContract(),event.getIdCustomer(),event.getType(),event.getMeasuresPoints(),event.getBeginDate()));
        }
        history.add(event);
    }
    
    public void apply(CustomerCreatedEvent event) {
    	this.id = event.getAggregateId();
    	this.idPartner = event.getIdPartener();
    	this.externalId = event.getExternalId();
    	history.add(event);
    }
    
    public void apply(CustomerChangeRegisterEvent event) {
    	if (this.contractList != null && event.getUri() != null) {
    		for (Contract contract : contractList) {
        		for (MeasurePoint measuresPoints : contract.getMeasuresPoints()) {
					for (Register register : measuresPoints.getRegister()) {
						if (event.getIdPartner().equals(register.getIdPartner()) && event.getIdRegistered().equals(register.getIdRegister())) {
	            			register.setUri(event.getUri());
	            			break;
						}
	            	}
				}
			}	
    		history.add(event);
    	}
        
    }

    public void apply(CustomerChangeEvent event) {
    	if (event.getIdPartener() != null) {
    		this.idPartner = event.getIdPartener();
    	}
    	if (event.getExternalId() != null) {
    		this.externalId = event.getExternalId();
    	}
        history.add(event);
    }
    
    public void apply(CustomerDeleteEvent event) {
    	deleted = true;
        history.add(event);
    }
}