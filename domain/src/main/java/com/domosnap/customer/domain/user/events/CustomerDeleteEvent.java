/* CustomerDeleteEvent
 * Event for when a customer is deleted
 * Raphaël Lopes
 * 06.06.2020
 */
package com.domosnap.customer.domain.user.events;

import com.domosnap.customer.domain.user.CustomerId;
import com.domosnap.tools.core.services.cqrs.domain.impl.EventBase;
import com.domosnap.tools.core.services.resourcenames.ResourceName;

public class CustomerDeleteEvent extends EventBase<CustomerId> {

    public CustomerDeleteEvent(CustomerId customerId, ResourceName creator) {
    	super(customerId, creator);
    }
}
