/* Register
 * this domain for the register
 * Raphaël Lopes
 * 06.06.2020
 */
package com.domosnap.customer.domain.user;

public class Register {
	
	private Integer idPartner;
	private String idRegister;
	private String uri;
	public Register(Integer idPartner,String idRegister,String uri)
	{
		this.idPartner = idPartner;
		this.idRegister = idRegister;
		this.uri = uri;
	}
	public Integer getIdPartner() {
		return idPartner;
	}
	public String getIdRegister() {
		return idRegister;
	}
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri= uri ;
	}
}
