/* MeasurePoint
 * this is the domain for the measurePoint
 * Raphaël Lopes
 * 06.06.2020
 */
package com.domosnap.customer.domain.user;

import java.util.List;

public class MeasurePoint {
	
	private Integer idPartner;
	private String idContract;
	private List<Register> registers;
	public MeasurePoint(Integer idPartner,String idContract,List<Register> registers) {
		this.idPartner = idPartner;
		this.idContract = idContract;
		this.registers = registers;
	}

	public Integer getIdPartner() {
		return idPartner;
	}

	public String getIdContract() {
		return idContract;
	}
	
	public List<Register> getRegister() {
		return registers;
	}
}
