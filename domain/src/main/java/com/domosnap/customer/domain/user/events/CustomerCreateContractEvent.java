/* CustomerCreateContractEvent
 * Event for when a contract is created
 * Raphaël Lopes
 * 06.06.2020
 */
package com.domosnap.customer.domain.user.events;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.domosnap.customer.domain.user.CustomerId;
import com.domosnap.customer.domain.user.MeasurePoint;
import com.domosnap.tools.core.services.cqrs.domain.impl.EventBase;
import com.domosnap.tools.core.services.resourcenames.ResourceName;

public class CustomerCreateContractEvent extends EventBase<CustomerId> {

	private Integer idPartner;
	private String idContract;
	private String idCustomer;
	private String type;
	private List<MeasurePoint> measuresPoints;
	private Date beginDate;
	private Optional<Date> mayBeEndDate;
    public CustomerCreateContractEvent(CustomerId customerId, Integer idPartner,String idContract,String idCustomer,String type,List<MeasurePoint> measuresPoints, Date beginDate, ResourceName creator) {
    	super(customerId, creator);
    	this.idPartner = idPartner;
		this.idContract = idContract;
		this.idCustomer = idCustomer;
		this.type = type;
		this.measuresPoints = measuresPoints;
		this.beginDate = beginDate;
		this.mayBeEndDate = Optional.empty();
    }

	public CustomerCreateContractEvent(CustomerId customerId, Integer idPartner,String idContract,String idCustomer,String type,List<MeasurePoint> measuresPoints, Date beginDate, Date endDate, ResourceName creator) {
    	this(customerId, idPartner, idContract, idCustomer, type, measuresPoints, beginDate, creator);
		this.mayBeEndDate = Optional.of(endDate);
    }

	public Integer getIdPartner() {
		return idPartner;
	}
	public String getIdContract() {
		return idContract;
	}
	public String getIdCustomer() {
		return idCustomer;
	}
	public String getType() {
		return type;
	}
	public List<MeasurePoint> getMeasuresPoints() {
		return measuresPoints;
	}
	public Date getBeginDate() {
		return beginDate;
	}
	public Optional<Date> getEndDate() {
		return this.mayBeEndDate;
	}

}
