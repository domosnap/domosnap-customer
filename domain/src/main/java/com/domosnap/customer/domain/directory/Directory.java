package com.domosnap.customer.domain.directory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import com.domosnap.customer.domain.directory.events.DirectoryContractAddedEvent;
import com.domosnap.customer.domain.directory.events.DirectoryContractRemovedEvent;
import com.domosnap.customer.domain.directory.events.DirectoryCreatedEvent;
import com.domosnap.customer.domain.directory.events.DirectoryCustomerAddedEvent;
import com.domosnap.customer.domain.directory.events.DirectoryCustomerRemovedEvent;
import com.domosnap.customer.domain.user.CustomerId;
import com.domosnap.tools.core.services.cqrs.domain.Aggregate;
import com.domosnap.tools.core.services.cqrs.domain.Event;
import com.domosnap.tools.core.services.cqrs.domain.impl.ProjectionBase;
import com.domosnap.tools.core.services.resourcenames.ResourceName;

public class Directory implements Aggregate<DirectoryId> {
    private Logger log = Logger.getLogger(Directory.class.getName());
    public final static DirectoryId mainDirectory = new DirectoryId("0");
	
    private DirectoryProjection projection;
    /**
     * This will create a directory
     * @param creator resource name
     * @param dispatchEvent consumer
     */
    public static void create(ResourceName creator, Consumer<Event<?>> dispatchEvent) {
    	dispatchEvent.accept(new DirectoryCreatedEvent(mainDirectory, creator));
    }
    
    public Directory(List<Event<?>> history) {
       projection = new DirectoryProjection(history);
    }

    public Directory() {
        this(new ArrayList<>());
    }

    /**
     * this will append when a user is added
     * @param smartDeviceId the smart device ID
     * @param creator resource name
     * @param dispatchEvent consumer
     */
    public void addCustomer(CustomerId customerId, String externalId, ResourceName creator, Consumer<Event<?>> dispatchEvent) {
    	Event<DirectoryId> event = new DirectoryCustomerAddedEvent(getId(), customerId, externalId, creator);
    	projection.apply(event);
    	dispatchEvent.accept(event);
    }

    public void addContract(CustomerId customerId, String contractId, ResourceName creator, Consumer<Event<?>> dispatchEvent) {
        Event<DirectoryId> event = new DirectoryContractAddedEvent(getId(), customerId, contractId, creator);
        projection.apply(event);
        dispatchEvent.accept(event);
    }

    public void removeContract(CustomerId customerId, String contractId, ResourceName creator, Consumer<Event<?>> dispatchEvent) {
        Event<DirectoryId> event = new DirectoryContractRemovedEvent(getId(), customerId, contractId, creator);
        projection.apply(event);
        dispatchEvent.accept(event);
    }
    
    /**
     * this will append when a user is removed
     * @param smartDeviceId the smart device ID
     * @param creator resource name
     * @param dispatchEvent consumer
     */
    public void removeCustomer(CustomerId customerId, ResourceName creator, Consumer<Event<?>> dispatchEvent) {
    	Event<DirectoryId> event = new DirectoryCustomerRemovedEvent(getId(), customerId, creator);
    	if (projection.customerById.containsKey(customerId)) {
    		projection.apply(event);
    		dispatchEvent.accept(event);
    	} else {
    		log.log(Level.FINEST, "Impossible to remove customer [{0}] since it isn''t contain in directory [{1}].", new Object[] {customerId, getId()});
    		throw new NoSuchElementException("No smartdevice [".concat(String.valueOf(customerId)).concat("] to delete."));
    	}
    }
    
    public DirectoryId getId() {
        return projection.id;
    }

    public List<CustomerId> getAllCustomerId() {
        return new ArrayList<>(projection.customerById.keySet());
    }
    
    
    public List<CustomerId> searchCustomers(Optional<String> mayBeExternalId, Optional<String> mayBeContractId) {
        
        List<CustomerId> customers = projection.customerById.keySet().stream()
			.filter(customerId -> {
				if(mayBeContractId.isPresent()) {
					Optional<CustomerId> mayBeCustomer = Optional.ofNullable(projection.customerByContract.get(mayBeContractId.get()));
					return mayBeCustomer.isPresent() && mayBeCustomer.get().getId().equals(customerId.getId());
				}
				return true;
			})
			.filter(customerId -> {
				if(mayBeExternalId.isPresent()) {
					Optional<CustomerId> mayBeCustomer = Optional.ofNullable(projection.customerByExternalId.get(mayBeExternalId.get()));
					return mayBeCustomer.isPresent() && mayBeCustomer.get().getId().equals(customerId.getId());
				}
				return true;
			}).collect(Collectors.toList());
        
        return customers;
    }
    
    public CustomerId getCustomerByExternalId(String externalId) {
    	return projection.customerByExternalId.get(externalId);
    }

    public CustomerId getCustomerByContractId(String contractId) {
        return projection.customerByContract.get(contractId);
    }

    public void clear() {
        projection.customerByContract.clear();
        projection.customerByExternalId.clear();
        projection.customerById.clear();
    }

    public void apply(Event<?> event) {
        projection.apply(event);
    }

    private class DirectoryProjection extends ProjectionBase {

    	public DirectoryId id;
    	public Map<CustomerId, String> customerById = new HashMap<>();
    	public Map<String, CustomerId> customerByExternalId = new HashMap<>();
        public Map<String, CustomerId> customerByContract = new HashMap<>();
    	
        public List<Event<?>> history = new ArrayList<>();

        public DirectoryProjection(List<Event<?>> history) {
            register(DirectoryCreatedEvent.class, this::apply);
            register(DirectoryCustomerAddedEvent.class, this::apply);
            register(DirectoryCustomerRemovedEvent.class, this::apply);
            register(DirectoryContractAddedEvent.class, this::apply);
            register(DirectoryContractRemovedEvent.class, this::apply);
            history.forEach(this::apply);
        }

        public void apply(DirectoryCreatedEvent event) {
        	this.id = event.getAggregateId();
        	history.add(event);
        }
        
        public void apply(DirectoryCustomerAddedEvent event) {
            customerById.put(event.getCustomerId(), event.getExternalId());
            customerByExternalId.put(event.getExternalId(), event.getCustomerId());
            history.add(event);
        }
        
        public void apply(DirectoryCustomerRemovedEvent event) {
            CustomerId customerId = event.getCustomerId();
            String externalId = customerById.get(customerId);
            customerById.remove(customerId);
            customerByExternalId.remove(externalId);
            history.add(event);
        }

        public void apply(DirectoryContractAddedEvent event) {
            customerByContract.put(event.getContractId(), event.getCustomerId());
            history.add(event);
        }

        public void apply(DirectoryContractRemovedEvent event) {
            String contractId = event.getContractId();
            customerByContract.remove(contractId);
            history.add(event);
        }
    }

}
