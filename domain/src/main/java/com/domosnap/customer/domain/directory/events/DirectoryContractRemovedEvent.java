package com.domosnap.customer.domain.directory.events;

import com.domosnap.customer.domain.directory.DirectoryId;
import com.domosnap.customer.domain.user.CustomerId;
import com.domosnap.tools.core.services.cqrs.domain.impl.EventBase;
import com.domosnap.tools.core.services.resourcenames.ResourceName;

public class DirectoryContractRemovedEvent extends EventBase<DirectoryId> {
    private CustomerId customerId;
    private String contractId;

    public DirectoryContractRemovedEvent(DirectoryId directoryId, CustomerId customerId, String contractId, ResourceName creator) {
        super(directoryId, creator);
        this.customerId = customerId;
        this.contractId = contractId;
    }

    public CustomerId getCustomerId() {
        return this.customerId;
    }

    public String getContractId() {
        return this.contractId;
    }
}