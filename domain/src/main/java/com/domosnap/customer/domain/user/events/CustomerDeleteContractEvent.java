/* CustomerDeleteContractEvent
 * Event for when a contract is deleted
 * Raphaël Lopes
 * 06.06.2020
 */
package com.domosnap.customer.domain.user.events;

import com.domosnap.customer.domain.user.CustomerId;
import com.domosnap.tools.core.services.cqrs.domain.impl.EventBase;
import com.domosnap.tools.core.services.resourcenames.ResourceName;

public class CustomerDeleteContractEvent extends EventBase<CustomerId> {

	private final Integer idPartner;
	private final String idContract;

    public CustomerDeleteContractEvent(CustomerId customerId, Integer idPartner, String idContract, ResourceName creator) {
    	super(customerId, creator);
        this.idPartner = idPartner;
        this.idContract = idContract;
    }

    public Integer getIdPartner() {
		return idPartner;
	}
    public String getIdContract() {
		return idContract;
	}

}
