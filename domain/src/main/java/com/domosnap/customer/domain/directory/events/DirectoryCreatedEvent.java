package com.domosnap.customer.domain.directory.events;

import com.domosnap.customer.domain.directory.DirectoryId;
import com.domosnap.tools.core.services.cqrs.domain.impl.EventBase;
import com.domosnap.tools.core.services.resourcenames.ResourceName;

public class DirectoryCreatedEvent extends EventBase<DirectoryId> {

	public DirectoryCreatedEvent(DirectoryId directoryId, ResourceName creator) {
		super(directoryId, creator);
	}
}
