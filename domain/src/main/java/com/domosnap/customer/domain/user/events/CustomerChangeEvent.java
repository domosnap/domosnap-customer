/* CustomerChangeEvent
 * Event for when a customer change
 * Raphaël Lopes
 * 06.06.2020
 */
package com.domosnap.customer.domain.user.events;

import com.domosnap.customer.domain.user.CustomerId;
import com.domosnap.tools.core.services.cqrs.domain.impl.EventBase;
import com.domosnap.tools.core.services.resourcenames.ResourceName;

public class CustomerChangeEvent extends EventBase<CustomerId> {

	private final Integer idPartner;
	private final String externalId;
	
	public CustomerChangeEvent(CustomerId customerId, Integer idPartner, String externalId, ResourceName creator) {
		super(customerId, creator);
		this.idPartner = idPartner;
		this.externalId = externalId;
	}

	public Integer getIdPartener() {
		return idPartner;
	}


	public String getExternalId() {
		return externalId;
	}
}
