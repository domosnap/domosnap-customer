/* Contract
 * This the domain for the contract
 * Raphaël Lopes
 * 08.06.2020
 */
package com.domosnap.customer.domain.user;
import java.util.Date;
import java.util.List;
import java.util.Optional;



public class Contract {
	
	private Integer idPartner;
	private String idContract;
	private String idCustomer;
	private String type;
	private List<MeasurePoint> measuresPoints;
	private Date beginDate;
	private Optional<Date> mayBeEndDate;
	//sur-charge au lieu du optional
	public Contract(Integer idPartner,String idContract,String idCustomer,String type,List<MeasurePoint>measuresPoints, Date beginDate) {
		this.idPartner = idPartner;
		this.idContract = idContract;
		this.idCustomer = idCustomer;
		this.measuresPoints = measuresPoints;
		this.beginDate = beginDate;
		this.mayBeEndDate = Optional.empty();
		this.type = type;
	}

	public Contract(Integer idPartner,String idContract,String idCustomer,String type,List<MeasurePoint>measuresPoints, Date beginDate, Date endDate) {
		this(idPartner, idContract, idCustomer, type, measuresPoints, beginDate);
		this.mayBeEndDate = Optional.of(endDate);
	}


	//primitive
	public Integer getIdPartner() {
		return this.idPartner;
	}
	public String getIdContract() {
		return this.idContract;
	}
	public String getIdCustomer() {
		return this.idCustomer;
	}
	public String getType() {
		return this.type;
	}
	public List<MeasurePoint> getMeasuresPoints() {
		return this.measuresPoints;
	}
	public Date getBeginDate() {
		return this.beginDate;
	}
	public Optional<Date> getEndDate() {
		return this.mayBeEndDate;
	}
}
