# Base sdk image include open jdk and maven
# FROM maven:3-jdk-13-alpine AS Build
# WORKDIR /usr/local/src
# COPY . .
# RUN ls /usr/local/src/rest-adapter
# RUN mvn --batch-mode clean package
# RUN ls /usr/local/src/rest-adapter
# RUN ls /usr/local/src/rest-adapter/target
#RUN mvn sonar:sonar \
#  -Dsonar.projectKey=Domosnap-Customer \
#  -Dsonar.host.url=https://sonarqube.org \
#  -Dsonar.login=fsdfasdfasdfasdfasfdasdf

# Final runtime image
FROM eclipse-temurin:19-jre-jammy as Final
WORKDIR /opt/customer
COPY ./infra/target/customer-infra-1.0-SNAPSHOT-runner.jar ./
RUN mv customer-infra-1.0-SNAPSHOT-runner.jar rest-adapter.jar
COPY ./infra/src/main/configuration/application.properties /app/config/

ENTRYPOINT ["java", "-Dquarkus.config.locations=/app/config/application.properties", "-Duser.dir=/tmp", "-jar", "customer-infra-1.0-SNAPSHOT-runner.jar"]
